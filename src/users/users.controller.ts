import { Body, Controller, Delete, Get, Param, Post, Put, UsePipes, ValidationPipe } from '@nestjs/common';


import { UsersService } from './users.service';

import { user } from './user.dto';

@Controller('users')
export class UsersController {

    constructor ( private userService : UsersService) {}

    @Get()
    async get()
    {
        return await this.userService .getAll();
    }


    @Get('/:id_user')
    getbyID(@Param('id_user') id_user)
    {
       return this.userService.findOne(id_user);
    }



   
    @Post()
    @UsePipes(ValidationPipe)
    async create(@Body() user : user)
    {
        return await this.userService.create(user);
    }



    @Put('/:id_user')
    async update(@Param('id_user') id_user, @Body() user){

        return await this.userService.update(id_user, user)

    }


    @Delete('/:id_user')
    async delete(@Param('id_user') id_user)
    {
        return await this.userService.delete(id_user)
    }
    



}
