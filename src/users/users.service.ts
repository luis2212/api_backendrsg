import { Injectable } from '@nestjs/common';

//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';

//Entidades
import { Users } from '../database/entitys/users.entity';
import {Role} from '../database/entitys/role.entity';
 


@Injectable()
export class UsersService {


    
    constructor(@InjectRepository(Users) private repoUser:Repository<Users>,
    @InjectRepository(Role) private repoRole:Repository<Role>){}

    async getAll()
    {
       try
        {
          return await this.repoUser.find({order : {'id_user': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }



    async findOne(id_user)
    {
        try{
        const founduser = await this.repoUser.findOne(id_user)
        
        if(!founduser) return {Message: 'User not found', Status: '404'}
        return {Message: founduser, Status:'201'}
    

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }

    }





    /*

        Json que recibe:

        {
            "username": "Prueba2",
            "name": "Ana", 
            "lastname": "Lenta",
            "password": "12345",
            "photo" :"",
            "role_id": 5
        }


    */


    
    async create(user)
    {
        try {  
            const newUser = new Users

            newUser.username = user.username;
            newUser.name = user.name;
            newUser.lastname = user.lastname;
            newUser.password= user.password
            newUser.photo = user.photo;
             
            //Rol
            const role = await this.repoRole.findOne(user.role_id)
            if(!role) return {Message: 'Role not found', Status: '404'}
            
            newUser.role = role
            const saveUser= await this.repoUser.save(newUser);

            return {Message: saveUser , Status:'201'}
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }

    }





    async update(id_user,user){

        try
        {
            //Buscamos usuarios
            const findUser  = await this.repoUser.findOne(id_user);
            if(!findUser) return {Message: 'User not found', Status: '404'}


            //Guardamos valores nuevos
            const newUser = new Users
            newUser.username = user.username;
            newUser.name = user.name;
            newUser.lastname = user.lastname;
            newUser.password= user.password
            newUser.photo = user.photo;


            //Comprobamos Rol
            const role = await this.repoRole.findOne(user.role_id)
            if(!role) return {Message: 'Role not found', Status: '404'}

            //Añadimos rol
            newUser.role = role

            //Unimos            
            this.repoUser.merge(findUser, newUser);

            //Objeto Actualizado
            const saveUser = await this.repoUser.save(findUser)
            return {'Message': saveUser , Status:'201'};

        
        } catch (error) {
         
            return {Message: 'Error', Status: '400'}
        }

    }




    async delete(id_user)
    {
        try
        {
            const deleteUser = await this.repoUser.delete(id_user);

            if(deleteUser.affected == 0) return {Message: 'User not found', Status: '404'}
        
            return {'Message': 'user deleted', Status:'201'};
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }





}
