import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';


import { Users } from '../database/entitys/users.entity';
import {Role  } from '../database/entitys/role.entity'
import { TypeOrmModule } from '@nestjs/typeorm';


@Module({
  imports:[TypeOrmModule.forFeature([Users, Role])],
  controllers: [UsersController],
  providers: [UsersService]
})
export class UsersModule {}
