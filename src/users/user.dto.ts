import { IsNotEmpty, Length } from "class-validator";

export class user{


    @IsNotEmpty({message: 'User must contain a user name'})
    @Length(3,25)   
    username : String;


    @IsNotEmpty({message: 'User must contain a name'})
    @Length(1,64) 
    name: String; 

    
    @IsNotEmpty({message: 'User must contain a name lastname'})
    @Length(1,64) 
    lastname: String; 


    @IsNotEmpty({message: 'User must contain a password'})
    @Length(5,254) 
    password: String;


    photo: String;


    role_id: number
          
}