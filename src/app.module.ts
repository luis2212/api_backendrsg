import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrivilegeGroupsModule } from './privilege-groups/privilege-groups.module';
import { PrivilegesModule } from './privileges/privileges.module';
import { RoleModule } from './role/role.module';
import { PermissionsModule } from './permissions/permissions.module';
import { UsersModule } from './users/users.module';
import { FavoriteBoardsModule } from './favorite-boards/favorite-boards.module';
import { DataGroupModule } from './data-group/data-group.module';
import { GroupModule } from './group/group.module';
import { BoardModule } from './board/board.module';
import { BoardMembersModule } from './board-members/board-members.module';
import { BoardModeratorModule } from './board-moderator/board-moderator.module';
import { ObjetivesModule } from './objetives/objetives.module';
import { RequerementsModule } from './requerements/requerements.module';
import { ScopeModule } from './scope/scope.module';
import { MettingTestsModule } from './metting-tests/metting-tests.module';
import { TicketModule } from './ticket/ticket.module';
import { BoardListsModule } from './board-lists/board-lists.module';
import { AccessCredentialsModule } from './access-credentials/access-credentials.module';
import { CredentialsCommentsModule } from './credentials-comments/credentials-comments.module';
import { LabelsModule } from './labels/labels.module';
import { ScrumCardModule } from './scrum-card/scrum-card.module';
import { LinkScardModule } from './link-scard/link-scard.module';
import { AttachmentsModule } from './attachments/attachments.module';
import { CardCommentsModule } from './card-comments/card-comments.module';
import { ChecklistModule } from './checklist/checklist.module';
import { TasksModule } from './tasks/tasks.module';



@Module({
  imports: [TypeOrmModule.forRoot(), PrivilegeGroupsModule, PrivilegesModule, RoleModule, PermissionsModule, UsersModule, FavoriteBoardsModule, DataGroupModule, GroupModule, BoardModule, BoardMembersModule, BoardModeratorModule, ObjetivesModule, RequerementsModule, ScopeModule, MettingTestsModule, TicketModule, BoardListsModule, AccessCredentialsModule, CredentialsCommentsModule, LabelsModule, ScrumCardModule, LinkScardModule, AttachmentsModule, CardCommentsModule, ChecklistModule, TasksModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
