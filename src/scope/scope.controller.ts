import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';

import { ScopeService } from './scope.service';

@Controller('scope')
export class ScopeController {


    constructor ( private scopeService : ScopeService) {}


    @Get()
    async get()
    {
        return await this.scopeService.getAll();
    }


    @Get('/:id_scope')
    getbyID(@Param('id_scope') id_scope)
    {
       return this.scopeService.findOne(id_scope);
    }



     //Se recibe el ID del Board   "Ver todos los alcanses de un proyecto"
     @Get('board/:id_board')
     getbyBoard(@Param('id_board') id_board)
     {
        return this.scopeService.findScopeBoard(id_board);
     }



     @Post()
     async create(@Body() scope)
     {
         return await this.scopeService.create(scope);
     }


     @Put('/:id_scope')
     async update(@Param('id_scope') id_scope, @Body() scope){
 
         return await this.scopeService.update(id_scope, scope)
 
     }


     @Delete('/:id_scope')
     async delete(@Param('id_scope') id_scope)
     {
         return await this.scopeService.delete(id_scope)
     }



     


}
