import { Injectable } from '@nestjs/common';

//Librerias
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

//Entidades
import { Scope } from '../database/entitys/scope.entity'
import { Board } from '../database/entitys/board.entity'


@Injectable()
export class ScopeService {

    constructor(@InjectRepository(Board) private repoBoard: Repository<Board>,
        @InjectRepository(Scope) private repoScope: Repository<Scope>) { }



    async getAll() {
        try {
            return await this.repoScope.find({ order: { 'id_scope': 'ASC' } });
        } catch (error) {
            return { Message: 'Error', Status: '400' }
        }
    }



    async findOne(id_scope) {
        try {
            const foundrole = await this.repoScope.findOne(id_scope)
            if (!foundrole) return { Message: 'Scope not found', Status: '404' }
            return { Message: foundrole, Status: '201' }

        } catch (error) {
            return { Message: 'Error', Status: '400' }
        }

    }




    //Todos los requerimeitnos de un Board
    // Ruta ->  http://localhost:3000/scope/board/1
    async findScopeBoard(id_board) {
        try {
            //Buscamos Board
            const findBoard = await this.repoBoard.findOne(id_board)
            if (!findBoard) return { Message: 'Board not found', Status: '404' }

            const foundFavorites = await this.repoScope.find({ where: { board: findBoard }, order: { 'id_scope': 'ASC' } })

            return { Message: foundFavorites, Status: '201' }

        } catch (error) {
            return { Message: 'Error', Status: '400' }
        }

    }







    /*
           JSON:
           {             
               "scope_description": "Precio fijo" , 
               "id_board" : 1
           }
       */

    async create(scope) {
        try {

            //Desestrucutramos
            const newScope = new Scope
            newScope.scope_description = scope.scope_description

            //Buscamos Board
            const findBoard = await this.repoBoard.findOne(scope.id_board)
            if (!findBoard) return { Message: 'Board not found', Status: '404' }
            newScope.board = findBoard


            const saveScope = await this.repoScope.save(newScope);

            return { Message: saveScope, Status: '201' }
        } catch (error) {
            return { Message: 'Error', Status: '400' }
        }
    }






    async update(id_scope, scope){

        try
        {
            const findScope  = await this.repoScope.findOne(id_scope);            
            if(!findScope) return {Message: 'Scope not found', Status: '404'}

            //Desestrucutramos
            const newScope = new Scope
            newScope.scope_description = scope.scope_description

            //Buscamos Board
            const findBoard = await this.repoBoard.findOne(scope.id_board)
            if (!findBoard) return { Message: 'Board not found', Status: '404' }
            newScope.board = findBoard


            this.repoScope.merge(findScope , newScope);

            //Objeto Actualizado
            const saveScope = await this.repoScope.save(findScope)
            return {'Message': saveScope , Status:'201'};

        
        } catch (error) {
         
            return {Message: 'Error', Status: '400'}
        }

    }



    async delete(id_scope)
    {
        try
        {
            const deleteRequets = await this.repoScope.delete(id_scope);

            if(deleteRequets.affected == 0) return {Message: 'Scope not found', Status: '404'}
        
            return {'Message': 'Scope deleted', Status:'201'};
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }





}
