import { Module } from '@nestjs/common';
import { ScopeController } from './scope.controller';
import { ScopeService } from './scope.service';

import { TypeOrmModule } from '@nestjs/typeorm';
import {Scope} from '../database/entitys/scope.entity'
import {Board} from '../database/entitys/board.entity'


@Module({
  imports:[TypeOrmModule.forFeature([Board, Scope])],
  controllers: [ScopeController],
  providers: [ScopeService]
})
export class ScopeModule {}
