import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Checklist } from 'src/database/entitys/checklist.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ChecklistService {
  constructor(
    @InjectRepository(Checklist)
    private checklistRepository: Repository<Checklist>,
  ) {}

  async getAllChecklist(): Promise<Checklist[]> {
    return this.checklistRepository.find();
  }

  async getOneChecklist(checklistId: number): Promise<Checklist> {
    return this.checklistRepository.findOne(checklistId);
  }

  async createChecklist(checklist: Checklist) {
    try {
      const newChecklist = await this.checklistRepository.create(checklist);
      const saveChecklist = await this.checklistRepository.save(newChecklist);

      return {
        Message: 'Checklist created successfully',
        Checklist: saveChecklist,
        Status: '201',
      };
    } catch (err) {
      return {
        Message: 'Error creating checklist',
        Error: err.message,
        Status: 400,
      };
    }
  }

  async deleteChecklist(checklistId: number) {
    try {
      const deleteChecklist = await this.checklistRepository.delete(
        checklistId,
      );
      return { Message: 'Checklist deleted successfully', Status: 201 };
    } catch (err) {
      return {
        Message: 'Error deleting checklist',
        Error: err.message,
        Status: 400,
      };
    }
  }

  async updateChecklist(checklistId: number, checklist: Checklist) {
    try {
      const foundChecklist = await this.checklistRepository.findOne(
        checklistId,
      );

      !foundChecklist
        ? { Message: 'Checklist not found', Status: 404 }
        : await this.checklistRepository.merge(foundChecklist, checklist);

      const saveChecklist = await this.checklistRepository.save(foundChecklist);

      return {
        Message: 'Checklist updated',
        Status: 201,
        checklist: saveChecklist,
      };
    } catch (err) {
      return {
        Message: 'Error updating checklist',
        Error: err.message,
        Status: 400,
      };
    }
  }
}
