import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checklist } from 'src/database/entitys/checklist.entity';
import { ChecklistController } from './checklist.controller';
import { ChecklistService } from './checklist.service';

@Module({
  imports:[TypeOrmModule.forFeature([Checklist])],
  controllers: [ChecklistController],
  providers: [ChecklistService]
})
export class ChecklistModule {}
