import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Checklist } from 'src/database/entitys/checklist.entity';
import { ChecklistService } from './checklist.service';

@Controller('checklist')
export class ChecklistController {
  constructor(private checklistService: ChecklistService) {}

  @Get()
  async getAllChecklist(): Promise<Checklist[]> {
    return await this.checklistService.getAllChecklist();
  }

  @Get('/:id')
  async getOneChecklist(@Param('id') id: number): Promise<Checklist> {
    return await this.checklistService.getOneChecklist(id);
  }

  @Post('/')
  async createChecklist(@Body() checklist: Checklist) {
    return await this.checklistService.createChecklist(checklist);
  }

  @Delete('/:id')
  async deleteChecklist(@Param('id') id_checklist: number) {
    return await this.checklistService.deleteChecklist(id_checklist);
  }

  @Put('/:id')
  async updateChecklist(
    @Param('id') id_checklist: number,
    @Body() checklist: Checklist,
  ) {
    return await this.checklistService.updateChecklist(id_checklist, checklist);
  }
}
