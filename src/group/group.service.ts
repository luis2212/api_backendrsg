import { Injectable } from '@nestjs/common';


//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';



//Entidades
import { Group } from '../database/entitys/group.entity';
import { Data_group } from 'src/database/entitys/data_group.entity';
import { Users } from 'src/database/entitys/users.entity';


@Injectable()
export class GroupService {

    
    constructor(@InjectRepository(Group) private repoGroup:Repository<Group>,
    @InjectRepository(Data_group) private repoDataGroup:Repository<Data_group>,
    @InjectRepository(Users) private repoUser:Repository<Users>){}


    async getAll()
    {
       try
        {
          return await this.repoGroup.find({order : {'group_id': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }



    async findOne(datagroup_id)
    {
        try{
       
        //Buscamos Group
        const datagroup = await this.repoDataGroup.findOne(datagroup_id) 
        if(!datagroup) return {Message: 'Data Group not found', Status: '404'}
        
    
        const foundMembers = await this.repoGroup.find({select:['users'] , where:{data_group: datagroup}})
        
 
        return {Message: foundMembers, Status:'201'}
        

        } catch (error) {
            return {Message: error, Status: '400'}
        }

    }



    /*

    Json que recibe:
        {
            "datagroup_id":1, 
            "id_user":2
        }
    */

    async create(Member)
    {
        try {  

            const member = new Group

            //Buscamos  Data grupo 
            
            const foundDatagroup = await this.repoDataGroup.findOne(Member.datagroup_id) 

            if(!foundDatagroup) return {Message: 'Data Group not found', Status: '404'}

            
            member.data_group = foundDatagroup;


            //Buscamos Usuario

            const foundUser = await this.repoUser.findOne(Member.id_user)
            
            if(!foundUser) return {Message: 'User not found', Status: '404'}
           
            member.users = foundUser
            
           //Guardamos
            const saveMember= await this.repoGroup.save(member);
            return {Message: saveMember , Status:'201'}

        } catch (error) {
            return {Message: error, Status: '400'}
        }
    }








    async delete(Member)
    {
        try
        {
           
            //Extraemos
            const member = new Group

            //Buscamos  Data grupo 
            
            const foundDatagroup = await this.repoDataGroup.findOne(Member.datagroup_id) 

            if(!foundDatagroup) return {Message: 'Data Group not found', Status: '404'}
            member.data_group = foundDatagroup;


            //Buscamos Usuario
            const foundUser = await this.repoUser.findOne(Member.id_user)
            if(!foundUser) return {Message: 'User not found', Status: '404'}
            member.users = foundUser

           
           
            const deleteMember = await this.repoGroup.delete(member)

            if(deleteMember.affected == 0) return {Message: 'Member not found', Status: '404'}
        
            return {'Message': 'Member deleted', Status:'201'};
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }

}
