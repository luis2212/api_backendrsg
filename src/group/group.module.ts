import { Module } from '@nestjs/common';
import { GroupService } from './group.service';

import {Group} from '../database/entitys/group.entity'
import { TypeOrmModule } from '@nestjs/typeorm';
import { GroupController } from './group.controller';
import { Data_group } from 'src/database/entitys/data_group.entity';
import { Users } from 'src/database/entitys/users.entity';


@Module({
    imports:[TypeOrmModule.forFeature([Group, Data_group, Users])],
    providers: [GroupService],
    controllers: [GroupController]
})
export class GroupModule {}
