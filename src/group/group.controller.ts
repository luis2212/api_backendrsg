import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';

import { GroupService } from './group.service';
 
@Controller('group')
export class GroupController {

    constructor ( private groupService : GroupService) {}


    @Get()
    async get()
    {
        return await this.groupService.getAll();
    }


    //Usuarios de un grupo
        // -> Recibe el ID del GRUPO  'data_group'

    @Get('/:datagroup_id')
    getbyID(@Param('datagroup_id') datagroup_id)
    {
        return this.groupService.findOne(datagroup_id);
    }




    @Post()
    async create(@Body() Member)
    {
        return await this.groupService.create(Member);
    }



    // No existe Actualizacón!!!!!!!!

            //Un usuario esta o no esta en un grupo

                //Si no es: elimina ese registro

    


    //Para eliminar 

            //Se deve recibir un Body con Equipo y Usuario

                    /*
                    {
                        "datagroup_id":1, 
                        "id_user":2
                    }
                    */

    @Delete()
    async delete(@Body() Member)
    {
      return await this.groupService.delete(Member)
    }

   

}
