import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Tasks } from 'src/database/entitys/tasks.entity';
import { TasksService } from './tasks.service';

@Controller('tasks')
export class TasksController {
  constructor(private tasksService: TasksService) {}

  @Get('/')
  async getAllTasks(): Promise<Tasks[]> {
    return await this.tasksService.getAllTasks();
  }

  @Get('/:id')
  async getOneTask(@Param('id') id: number): Promise<Tasks> {
    return await this.tasksService.getOneTask(id);
  }

  @Post('/')
  async createTask(@Body() task: Tasks) {
    return await this.tasksService.createTask(task);
  }

  @Delete('/:id')
  async deleteTask(@Param('id') id: number) {
    return await this.tasksService.deleteTask(id);
  }

  @Put('/:id')
  async updateTask(@Param('id') id: number, @Body() task: Tasks) {
    return await this.tasksService.updateTask(id, task);
  }
}
