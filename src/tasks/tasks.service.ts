import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Tasks } from 'src/database/entitys/tasks.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Tasks) private taskRepository: Repository<Tasks>,
  ) {}

  async getAllTasks(): Promise<Tasks[]> {
    return await this.taskRepository.find();
  }

  async getOneTask(id_task: number): Promise<Tasks> {
    return await this.taskRepository.findOne(id_task);
  }

  async createTask(task: Tasks) {
    try {
      const newTask = await this.taskRepository.create(task);
      const saveTask = await this.taskRepository.save(newTask);

      return {
        Message: 'Task created successfully',
        Task: saveTask,
        Status: 201,
      };
    } catch (err) {
      return {
        Message: 'Error creating task',
        Error: err.message,
        Status: 400,
      };
    }
  }

  async deleteTask(taskId: number) {
    try {
      const deleteTask = await this.taskRepository.delete(taskId);

      return { Message: 'Task deleted successfully', Status: 201 };
    } catch (err) {
      return {
        Message: 'Error deleting task',
        Error: err.message,
        Status: 400,
      };
    }
  }

  async updateTask(taskId: number, task: Tasks) {
    try {
      const foundTask = await this.taskRepository.findOne(taskId);
      !foundTask
        ? { Message: 'Task not found', Status: 404 }
        : await this.taskRepository.merge(foundTask, task);

      const saveTask = await this.taskRepository.save(foundTask);

      return {
        Message: 'Task updated successfully',
        NewTask: saveTask,
        Status: 201,
      };
    } catch (err) {
      return {
        Message: 'Error updating task',
        Error: err.message,
        Status: 400,
      };
    }
  }
}
