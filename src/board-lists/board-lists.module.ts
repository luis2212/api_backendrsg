import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Board_lists } from 'src/database/entitys/board_lists.entity';
import { BoardListsController } from './board-lists.controller';
import { BoardListsService } from './board-lists.service';

@Module({
  imports: [TypeOrmModule.forFeature([Board_lists])],
  controllers: [BoardListsController],
  providers: [BoardListsService]
})
export class BoardListsModule {}
