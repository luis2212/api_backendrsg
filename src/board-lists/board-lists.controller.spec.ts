import { Test, TestingModule } from '@nestjs/testing';
import { BoardListsController } from './board-lists.controller';

describe('BoardListsController', () => {
  let controller: BoardListsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BoardListsController],
    }).compile();

    controller = module.get<BoardListsController>(BoardListsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
