import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { Board_lists } from 'src/database/entitys/board_lists.entity';
import { BoardListsService } from './board-lists.service';

@Controller('board-lists')
export class BoardListsController {
  constructor(private board_listsService: BoardListsService){}

  @Get('/')
  async getAllBoardLists(): Promise<Board_lists[]>{
    return await this.board_listsService.getBoardLists();
  }

  @Get('/:id')
  async getBoardList(@Param('id') id: number): Promise<Board_lists>{
    return await this.board_listsService.getOneBoardList(id);
  }

  @Post('/')
  async createBoardList(@Body() body: Board_lists){
    return await this.board_listsService.createBoardList(body);
  }

  @Delete('/:id')
  async deleteBoardList(@Param('id') id: number){
    return await this.board_listsService.deleteBoardList(id);
  }

  @Put('/:id')
  async updateBoardList(@Param('id') id: number, @Body() body: Board_lists){
    return await this.board_listsService.updateBoardList(id, body)
  }

}
