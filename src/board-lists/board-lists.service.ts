import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Board } from 'src/database/entitys/board.entity';
import { Board_lists } from 'src/database/entitys/board_lists.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BoardListsService {
  constructor(
    @InjectRepository(Board_lists)
    private board_listsRepository: Repository<Board_lists>,
  ) {}

  async getBoardLists(): Promise<Board_lists[]> {
    return await this.board_listsRepository.find();
  }

  async getOneBoardList(id_board_list: number): Promise<Board_lists> {
    return await this.board_listsRepository.findOne(id_board_list);
  }

  async createBoardList(body: Board_lists) {
    try {
      const newBoardList = await this.board_listsRepository.create(body);
      const saveBoardList = await this.board_listsRepository.save(newBoardList);

      return { Board_lists: saveBoardList, Status: 201 };
    } catch (err) {
      return { Message: err.Message, Status: 400 };
    }
  }

  async deleteBoardList(id_board_list) {
    try {
      const deleteBoardList = await this.board_listsRepository.delete(
        id_board_list,
      );
      return { Message: 'Board lists deleted successfully', Status: 201 };
    } catch (err) {
      return { Message: err.Message, Status: 400 };
    }
  }

  async updateBoardList(id_board_list, body: Board_lists) {
    try {
      const foundBoardList = await this.board_listsRepository.findOne(
        id_board_list,
      );
      !foundBoardList
        ? { Message: 'Board List Not Found', Status: 404 }
        : await this.board_listsRepository.merge(foundBoardList, body);

      const updateBoardList = await this.board_listsRepository.save(
        foundBoardList,
      );
      return {
        Message: 'Boardlists uploaded sucefully',
        Boardlists: updateBoardList,
        Status: 201,
      };
    } catch (err) {
      return { Message: 'Error to update board list', Status: 400 };
    }
  }
}
