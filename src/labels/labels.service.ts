import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Labels } from 'src/database/entitys/labels.entity';
import { Repository } from 'typeorm';

@Injectable()
export class LabelsService {
  constructor(
    @InjectRepository(Labels) private labelsRepository: Repository<Labels>,
  ) {}

  async getAllLabels(): Promise<Labels[]> {
    return this.labelsRepository.find();
  }

  async getOneLabel(id_label: number): Promise<Labels> {
    return this.labelsRepository.findOne(id_label);
  }

  async createLabel(body: Labels) {
    try {
      const newLabels = await this.labelsRepository.create(body);
      const labelCreated = await this.labelsRepository.save(newLabels);

      return {
        Message: 'Label created successfully.',
        LabelCreated: labelCreated,
        Status: 201,
      };
    } catch (err) {
      return { Message: err.message, Status: 400 };
    }
  }

  async deleteLabel(id_label: number) {
    try {
      const labelDelete = await this.labelsRepository.delete(id_label);
      return { Message: 'Label deleted successfully', Status: 201 };
    } catch (err) {
      return { Message: err.message, Status: 400 };
    }
  }

  async updateLabel(id_label: number, body: Labels) {
    try {
      const foundLabel = await this.labelsRepository.findOne(id_label);
      !foundLabel
        ? { Message: "Label not found', Status: 404'" }
        : await this.labelsRepository.merge(foundLabel, body);

      const updatedLabel = await this.labelsRepository.save(foundLabel);

      return {
        Message: 'Label updated successfully',
        Status: 201,
        NewLabel: updatedLabel,
      };
    } catch (err) {
      return { Message: err.message, Status: 400 };
    }
  }
}
