import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Labels } from 'src/database/entitys/labels.entity';
import { LabelsService } from './labels.service';

@Controller('labels')
export class LabelsController {
  constructor(private labelsService: LabelsService) {}

  @Get()
  async getAllLabels(): Promise<Labels[]> {
    return await this.labelsService.getAllLabels();
  }

  @Get('/:id')
  async getOneLabel(@Param('id') id: number): Promise<Labels> {
    return await this.labelsService.getOneLabel(id);
  }

  @Post()
  async createLabel(@Body() body: Labels) {
    return await this.labelsService.createLabel(body);
  }

  @Delete('/:id')
  async deleteLabel(@Param('id') id: number) {
    return await this.labelsService.deleteLabel(id);
  }

  @Put('/:id')
  async updateLabel(@Param('id') id: number, @Body() body: Labels) {
    return await this.labelsService.updateLabel(id, body);
  }
}
