import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Access_credentials } from 'src/database/entitys/access_credentials.entity';
import { AccessCredentialsService } from './access-credentials.service';

@Controller('access-credentials')
export class AccessCredentialsController {
  constructor(private accessCredentialsService: AccessCredentialsService) {}

  @Get('/')
  async getAllAccessCredentials(): Promise<Access_credentials[]> {
    return await this.accessCredentialsService.getAllAccessCredentials();
  }

  @Get('/:id')
  async getAccessCredentials(
    @Param('id') id: number,
  ): Promise<Access_credentials> {
    return await this.accessCredentialsService.getOneAccessCredentials(id);
  }

  @Post('/')
  async createAccessCredentials(@Body() body: Access_credentials) {
    return await this.accessCredentialsService.createAccessCredentials(body);
  }

  @Delete('/:id')
  async deleteAccess_credentials(@Param('id') id_access_credentials: number) {
    return await this.accessCredentialsService.deleteAccess_credentials(
      id_access_credentials,
    );
  }

  @Put('/:id')
  async updateAccessCredentials(
    @Param('id') id: number,
    @Body() body: Access_credentials,
  ) {
    return await this.accessCredentialsService.updateAccess_credentials(
      id,
      body,
    );
  }
}
