import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Access_credentials } from 'src/database/entitys/access_credentials.entity';
import { Users } from 'src/database/entitys/users.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AccessCredentialsService {
  constructor(
    @InjectRepository(Access_credentials)
    private Access_credentialsRespository: Repository<Access_credentials>,
    @InjectRepository(Users) private repoUser: Repository<Users>,
  ) {}

  async getAllAccessCredentials(): Promise<Access_credentials[]> {
    return await this.Access_credentialsRespository.find();
  }

  async getOneAccessCredentials(
    id_access_credentials,
  ): Promise<Access_credentials> {
    return await this.Access_credentialsRespository.findOne(
      id_access_credentials,
    );
  }

  async createAccessCredentials(access_credentials: Access_credentials) {
    try {
      const newAccessCredentials =
        await this.Access_credentialsRespository.create(access_credentials);

      //users_access
      if (
        access_credentials.users_access &&
        access_credentials.users_access.length > 0
      ) {
        let Users = [];

        for (let i = 0; i < access_credentials.users_access.length; i++) {
          const idUser = access_credentials.users_access[i];
          const newUser = await this.repoUser.findOne(idUser);
          if (!newUser) return { Message: 'User not found', Status: 404 };
          Users.push(newUser);
        }
        newAccessCredentials.users_access = Users;
      }
      const Access_credentialsCreated =
        await this.Access_credentialsRespository.save(newAccessCredentials);
      return {
        Message: 'Access Credentials Created',
        Access_credentials: Access_credentialsCreated,
        Status: 201,
      };
    } catch (err) {
      return { Message: 'Something Wrong happened', Status: 400 };
    }
  }

  async deleteAccess_credentials(id_access_credentials) {
    try {
      const deleteAccess_credentials =
        await this.Access_credentialsRespository.delete(id_access_credentials);
      return {
        Message: 'Access Credentials deleted successfully',
        Status: 201,
      };
    } catch (e) {
      return { Message: 'Something wrong', Status: 400 };
    }
  }

  async updateAccess_credentials(
    id_access_credentials: number,
    body: Access_credentials,
  ) {
    try {
      const foundAccess_credentials =
        await this.Access_credentialsRespository.findOne(id_access_credentials);
      if (body.users_access && body.users_access.length > 0) {
        let Users = [];

        for (let i = 0; i < body.users_access.length; i++) {
          const idUser = body.users_access[i];
          const newUser = await this.repoUser.findOne(idUser);
          if (!newUser) return { Message: 'User not found', Status: 404 };
          Users.push(newUser);
        }
        foundAccess_credentials.users_access = Users;
      }
      if (foundAccess_credentials) {
        const mergeAccess_credentials =
          await this.Access_credentialsRespository.merge(
            foundAccess_credentials,
            body,
          );
        body.id_board = mergeAccess_credentials.id_board;
        const updateAccess_credentials =
          await this.Access_credentialsRespository.save(
            mergeAccess_credentials,
          );

        return {
          Message: 'Access credentials updated successfully',
          newAccessCredentials: updateAccess_credentials,
          Status: 201,
        };
      } else {
        return { Message: 'Access credentials not updated', Status: 404 };
      }
    } catch (err) {
      return { Message: 'Something Wrong happened', Status: 400 };
    }
  }
}
