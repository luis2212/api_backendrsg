import { Test, TestingModule } from '@nestjs/testing';
import { AccessCredentialsService } from './access-credentials.service';

describe('AccessCredentialsService', () => {
  let service: AccessCredentialsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AccessCredentialsService],
    }).compile();

    service = module.get<AccessCredentialsService>(AccessCredentialsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
