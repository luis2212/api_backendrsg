import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Access_credentials } from 'src/database/entitys/access_credentials.entity';
import { Users } from 'src/database/entitys/users.entity';
import { AccessCredentialsController } from './access-credentials.controller';
import { AccessCredentialsService } from './access-credentials.service';

@Module({
  imports: [TypeOrmModule.forFeature([Access_credentials, Users])],
  controllers: [AccessCredentialsController],
  providers: [AccessCredentialsService],
})
export class AccessCredentialsModule {}
