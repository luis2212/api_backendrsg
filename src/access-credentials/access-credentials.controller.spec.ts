import { Test, TestingModule } from '@nestjs/testing';
import { AccessCredentialsController } from './access-credentials.controller';

describe('AccessCredentialsController', () => {
  let controller: AccessCredentialsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AccessCredentialsController],
    }).compile();

    controller = module.get<AccessCredentialsController>(AccessCredentialsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
