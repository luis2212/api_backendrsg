import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';


import { DataGroupService } from './data-group.service';

@Controller('data_group')
export class DataGroupController {

    constructor ( private dataGroupService : DataGroupService) {}


    @Get()
    async get()
    {
        return await this.dataGroupService.getAll();
    }

    @Get('/:datagroup_id')
    getbyID(@Param('datagroup_id') datagroup_id)
    {
       return this.dataGroupService.findOne(datagroup_id);
    }


    /*
    Json que recibe:
    
     {
        "datagroup_name": "Panditas",
        "datagroup_photo":""
     }

    */


    @Post()
    async create(@Body() DataGroup)
    {
        return await this.dataGroupService.create(DataGroup);
    }




    @Put('/:datagroup_id')
    async update(@Param('datagroup_id') datagroup_id, @Body() DataGroup){

        return await this.dataGroupService.update(datagroup_id, DataGroup)

    }



    @Delete('/:datagroup_id')
    async delete(@Param('datagroup_id') datagroup_id)
    {
        return await this.dataGroupService.delete(datagroup_id)
    }


    
}
