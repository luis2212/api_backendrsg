import { Injectable } from '@nestjs/common';


//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';

//Entidades
import { Data_group } from '../database/entitys/data_group.entity';




@Injectable()
export class DataGroupService {


    
    constructor(@InjectRepository(Data_group) private repoDataGroup:Repository<Data_group>){}


    async getAll()
    {
       try
        {
          return await this.repoDataGroup.find({order : {'datagroup_id': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }


    
    async findOne(datagroup_i)
    {
        try{
        const foundDataGroup = await this.repoDataGroup.findOne(datagroup_i)
        
        if(!foundDataGroup) return {Message: 'Data Group not found', Status: '404'}
        
        return {Message: foundDataGroup, Status:'201'}
        

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }






    async create(DataGroup)
    {
        try {  
            const newData= this.repoDataGroup.create(DataGroup);
            const saveData= await this.repoDataGroup.save(newData);
            
            return {Message: saveData , Status:'201'}
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }






    async update(datagroup_id, DataGroup){

        try
        {
            const findData  = await this.repoDataGroup.findOne(datagroup_id);
            
            if(!findData) return {Message: 'Data Group not found', Status: '404'}

            this.repoDataGroup.merge(findData, DataGroup);

            //Objeto Actualizado
            const saveData = await this.repoDataGroup.save(findData)
            return {'Message': saveData , Status:'201'};
        
        } catch (error) {
         
            return {Message: 'Error', Status: '400'}
        }

    }






    async delete(datagroup_id)
    {
        try
        {
            const deleteData = await this.repoDataGroup.delete(datagroup_id);

            if(deleteData.affected == 0) return {Message: 'Data Group not found', Status: '404'}
        
            return {'Message': 'Data Group deleted', Status:'201'};
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }

}
