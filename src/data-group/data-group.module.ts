import { Module } from '@nestjs/common';
import { DataGroupController } from './data-group.controller';
import { DataGroupService } from './data-group.service';



import { Data_group } from '../database/entitys/data_group.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports:[TypeOrmModule.forFeature([Data_group])],
  controllers: [DataGroupController],
  providers: [DataGroupService]
})
export class DataGroupModule {}
