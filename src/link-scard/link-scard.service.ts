import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Link_scard } from 'src/database/entitys/link_scard.entity';
import { Repository } from 'typeorm';

@Injectable()
export class LinkScardService {
  constructor(
    @InjectRepository(Link_scard)
    private link_scardRepository: Repository<Link_scard>,
  ) {}

  async getAllLinkScard(): Promise<Link_scard[]> {
    return this.link_scardRepository.find();
  }

  async getOneLinkScard(id_link: number): Promise<Link_scard> {
    return this.link_scardRepository.findOne(id_link);
  }

  async createLinkScard(body: Link_scard) {
    try {
      const newLinkScard = await this.link_scardRepository.create(body);
      const LinkScardCreated = await this.link_scardRepository.save(
        newLinkScard,
      );

      return {
        Message: ' Link Card created successfully',
        linkCreated: LinkScardCreated,
        Status: 201,
      };
    } catch (err) {
      return {
        Message: 'Error creating link scard',
        Error: err.message,
        Status: 400,
      };
    }
  }

  async deleteLinkScard(id_link: number) {
    try {
      const deleteLinkScard = await this.link_scardRepository.delete(id_link);
      return { Message: 'Delete link scard successfully', Status: 201 };
    } catch (err) {
      return {
        Message: 'Error deleting link scard',
        Error: err.message,
        Status: 400,
      };
    }
  }

  async updateLinkScard(id_link: number, body: Link_scard) {
    try {
      const foundLinkScard = await this.link_scardRepository.findOne(id_link);
      !foundLinkScard
        ? { Message: 'Link Scard not found', Status: 404 }
        : await this.link_scardRepository.merge(foundLinkScard, body);
      const updatedLinkScard = await this.link_scardRepository.save(
        foundLinkScard,
      );
      

      return {
        Message: 'Link Scard updated successfully',
        Status: 200,
        updatedLinkScard: updatedLinkScard,
      };
    } catch (err) {
      return {
        Message: 'Error updating link scard',
        Error: err.message,
        Status: 400,
      };
    }
  }
}
