import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Link_scard } from 'src/database/entitys/link_scard.entity';
import { LinkScardController } from './link-scard.controller';
import { LinkScardService } from './link-scard.service';

@Module({
  imports: [TypeOrmModule.forFeature([Link_scard])],
  controllers: [LinkScardController],
  providers: [LinkScardService],
})
export class LinkScardModule {}
