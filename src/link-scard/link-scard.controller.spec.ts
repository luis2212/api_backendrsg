import { Test, TestingModule } from '@nestjs/testing';
import { LinkScardController } from './link-scard.controller';

describe('LinkScardController', () => {
  let controller: LinkScardController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LinkScardController],
    }).compile();

    controller = module.get<LinkScardController>(LinkScardController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
