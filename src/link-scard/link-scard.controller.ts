import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Link_scard } from 'src/database/entitys/link_scard.entity';
import { LinkScardService } from './link-scard.service';

@Controller('link-scard')
export class LinkScardController {
  constructor(private linkScardService: LinkScardService) {}

  @Get()
  async getAllLinkScard(): Promise<Link_scard[]> {
    return await this.linkScardService.getAllLinkScard();
  }

  @Get('/:id')
  async getOneLinkScard(@Param('id') id_link: number): Promise<Link_scard> {
    return await this.linkScardService.getOneLinkScard(id_link);
  }

  @Post()
  async createLinkScard(@Body() body: Link_scard) {
    return await this.linkScardService.createLinkScard(body);
  }

  @Delete('/:id')
  async deleteLinkScard(@Param('id') id: number) {
    return await this.linkScardService.deleteLinkScard(id);
  }

  @Put('/:id')
  async updateLinkScard(@Param('id') id: number, @Body() body: Link_scard) {
    return await this.linkScardService.updateLinkScard(id, body);
  }
}
