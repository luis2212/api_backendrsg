import { Test, TestingModule } from '@nestjs/testing';
import { LinkScardService } from './link-scard.service';

describe('LinkScardService', () => {
  let service: LinkScardService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LinkScardService],
    }).compile();

    service = module.get<LinkScardService>(LinkScardService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
