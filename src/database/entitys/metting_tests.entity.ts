import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, ManyToMany, JoinTable} from 'typeorm';

import { Board } from './board.entity'; 
import { Users } from './users.entity';


@Entity()
export class Metting_tests{

  @PrimaryGeneratedColumn()
  id_metting: number;


  @ManyToOne( () => Board, (board) => board.metting_test , {eager:true} )
  @JoinColumn({name: 'id_board'})  
  board: Board;


  @Column("varchar",{length:35})
  title_metting: string;

  @Column("varchar",{length:60})
  description_metting: string;


  @Column("date")
  scheduled_date: Date;


  @Column({ type: 'timestamptz', nullable: true })
  start_time: Date;

  @Column({ type: 'timestamptz', nullable: true })
  end_time: Date;
  
   @Column("varchar", {length:30})
   platform: String;
  
   @Column("text")
   memorandum: String;
  
   @Column("boolean", {default:false})
   archived_metting : boolean;


  @ManyToMany(()=>Users, users => users.mettings)
  @JoinTable(
    { name: "metting_attendees", 
      joinColumn:{
        name:'id_metting'
      },
      inverseJoinColumn:
      {
        name:'id_user'
      }  
    })
  attendees : Users[];

}