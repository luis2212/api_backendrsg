import { Entity, Column, PrimaryColumn, ManyToOne, JoinColumn, Generated} from 'typeorm';

import { Users } from './users.entity';
import { Board } from './board.entity';

@Entity()
export class Board_moderator{

  @Column({unique:true})
  @Generated('increment')
  moderator_id: number;


  @ManyToOne(() => Board , board => board.board_moderator , {primary:true, eager : true})
  @JoinColumn({name : 'id_board'}) 
  board: number;


  @ManyToOne(() => Users , users => users.board_moderator , {primary:true ,eager : true})
  @JoinColumn({name : 'id_user'})
  users: number;

}