
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, OneToOne, JoinColumn, OneToMany} from "typeorm";
import { Scrum_card } from "./scrum_card.entity";
import { Tasks } from "./tasks.entity";
import { Users } from "./users.entity";

@Entity()
export class Checklist {

    @PrimaryGeneratedColumn({type:"int"})
    id_checklist: number;

    @ManyToOne(type=>Scrum_card, scard=> scard.checklists)
    @JoinColumn({name : 'id_scard'})
    id_scard:Scrum_card;

    @Column({type:"varchar", length:40})
    checklist_name:string;

    @OneToOne(()=> Users,{
      eager:true
    })
    @JoinColumn({name:'id_user'})
    user: Users;

    @OneToMany(()=> Tasks, tasks => tasks.id_checklist)
    tasks: Tasks[];
}