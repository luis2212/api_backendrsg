import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne, Generated} from 'typeorm';


import { Users } from './users.entity';
import { Board } from './board.entity';

@Entity()
export class Favorite_boards {
  
  @Column({unique:true})
  @Generated('increment')
  id_favorite: number;
  
 
  @ManyToOne(() => Users , users => users.favorite_boards, {primary:true , eager : true})
  @JoinColumn({name : 'id_user'})
  users: Users;


  @ManyToOne(() => Board , board => board.favorite_boards, {primary:true,eager : true})
  @JoinColumn({name : 'id_board'})
  Board: Board;

}