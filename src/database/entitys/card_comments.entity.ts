
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, OneToOne, JoinColumn} from "typeorm";
import { Scrum_card } from "./scrum_card.entity";
import { Users } from "./users.entity";

@Entity()
export class Card_comments {

    @PrimaryGeneratedColumn({type:"int"})
    id_comment: number;


    @Column({type:"varchar", length:100})
    comment_content:string;


    @ManyToOne(type=>Scrum_card, scard=> scard.comments)
    @JoinColumn({name : 'id_scard'})
    id_scard:Scrum_card;


    @ManyToOne(type=> Users, users=> users.card_comments, {eager:true})
    @JoinColumn({name : 'id_user'})
    users:Scrum_card;


}