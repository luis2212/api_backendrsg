import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';

import { Board } from './board.entity'; 


@Entity()
export class Scope {

  @PrimaryGeneratedColumn()
  id_scope: number;


  @ManyToOne( () => Board, (board) => board.scope , {eager:true} )
  @JoinColumn({name: 'id_board'})  
  board: Board;


  @Column("varchar",{length:100})
  scope_description: string;


}