
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne,JoinTable, JoinColumn} from "typeorm";
import { Board } from "./board.entity";
import { Users } from "./users.entity";

@Entity()
export class Ticket {

    @PrimaryGeneratedColumn({type:"int"})
    id_ticket: number;

    @ManyToOne(type=>Board, board=> board.tickets,{eager:true})
    @JoinColumn({name : 'id_board'})
    id_board:Board;

    @Column({type:"varchar", length:40})
    ticket_tittle:string;

    @Column({type:"varchar", length:20})
    color: string; 

    @Column({type:"varchar", length:150})
    ticket_description:string;

    @Column({default:false})
    ticket_status: boolean;

    @Column({type:"date"})
    customer_date: Date;

    @Column({type:"date"})
    resolution_date: Date;

    @Column({type:"time"})
    ticket_time: string;

    @Column({type: "varchar", length: 250})
    solution: string;

    @Column({type:"boolean"})
    archived_ticket: boolean;

    @ManyToMany(()=> Users, users => users.tickets, {eager : true})
    @JoinTable(
        { name: "tickets_attendees", 
          joinColumn:{
            name:'id_ticket'
          },
          inverseJoinColumn:
          {
            name:'id_user'
          }  
        })
    attendees: Users[];
}