import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';



import { Board } from './board.entity'; 


@Entity()
export class Objetive {

  @PrimaryGeneratedColumn()
  id_objetive: number;

    
  @ManyToOne( () => Board, (board) => board.objetive , {eager:true} )
  @JoinColumn({name: 'id_board'})  
  board: Board


  @Column("varchar",{length:100})
  objetive_tittle: string;


  //¿Se cumplio?
  @Column("boolean", {default : false})
  target_status: boolean


}