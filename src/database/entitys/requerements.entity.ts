import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';

import { Board } from './board.entity'; 


@Entity()
export class Requerements {

  @PrimaryGeneratedColumn()
  id_request: number;


  @ManyToOne( () => Board, (board) => board.requerements, {eager:true} )
  @JoinColumn({name: 'id_board'})  
  board: Board;


  @Column("varchar",{length:100})
  request_description: string;


  //¿Se cumplio?
  @Column("boolean", {default : false})
  request_status: boolean;


}