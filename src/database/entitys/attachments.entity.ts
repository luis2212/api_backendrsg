import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Scrum_card } from './scrum_card.entity';

@Entity()
export class Attachments {
  @PrimaryGeneratedColumn({ type: 'int' })
  id_attachment: number;

  @ManyToOne((type) => Scrum_card, (scard) => scard.attachments)
  @JoinColumn({name : 'id_scard'})
  id_scard: Scrum_card;

  @Column({ type: 'text' })
  attachment_filename: string;

  @Column({ type: 'varchar', length: 25})
  attachment_name: string;
}
