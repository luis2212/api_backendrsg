
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, OneToMany, JoinTable, JoinColumn} from "typeorm";
import { Attachments } from "./attachments.entity";
import { Board_lists } from "./board_lists.entity";
import { Card_comments } from "./card_comments.entity";
import { Checklist } from "./checklist.entity";
import { Labels } from "./labels.entity";
import { Link_scard } from "./link_scard.entity";
import { Users } from "./users.entity";

@Entity()
export class Scrum_card {

    @PrimaryGeneratedColumn({type:"int"})
    id_scard: number;

    @ManyToOne(type=>Board_lists, blist=> blist.scards)
    @JoinColumn({name : 'id_list'})
    id_list:Board_lists;

    @Column({type:"varchar", length:30})
    scard_tittle:string;

    @Column({type:"text"})
    image_cover: string; 

    @Column({type:"varchar", length:15})
    color:string;

    @Column({type:"varchar", length:250})
    scard_description:string; string;

    @Column({type:"date"})
    start_date: Date;

    @Column({type:"date"})
    end_date: Date;

    @Column({type:"boolean"})
    scard_status:boolean;

    @Column({type:"timestamp"})
    start_date_devop: string;

    @Column({type: "timestamp"})
    end_date_devop: string;

    @Column({type:"time"})
    dedicated_time: string;

    @Column({type:"boolean"})
    archived_scard: boolean;

    @ManyToMany(()=>Labels, label =>label.scards)
    @JoinTable(
        { name: "labels_scards", 
          joinColumn:{
            name:'id_scard'
          },
          inverseJoinColumn:
          {
            name:'id_label'
          }  
        })
    labels: Labels[];

    @OneToMany(()=>Link_scard, lcard => lcard.id_scard, { eager:true})
    links: Link_scard[];

    @OneToMany(()=> Attachments, attachments => attachments.id_scard, { eager:true})
    attachments: Attachments[];

    @ManyToMany(()=> Users, user => user.scards,{ eager:true })
    @JoinTable(
        { name: "card_keepers", 
          joinColumn:{
            name:'id_scard'
          },
          inverseJoinColumn:
          {
            name:'id_user'
          }  
        })
    keppers: Users[];

    @OneToMany(()=>Card_comments, comments=> comments.id_scard, { eager:true})
    comments: Card_comments[];

    @OneToMany(()=>Checklist, checklists => checklists.id_scard, { eager:true})
    checklists: Checklist[];
}