import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  OneToOne,
} from 'typeorm';

import { Permissions } from './permissions.entity';
import { Users } from './users.entity';

@Entity()
export class Role {
  @PrimaryGeneratedColumn()
  role_id: number;

  @Column('character varying', { length: 20, unique: true, nullable: false })
  role_code: string;

  @Column('character varying', { length: 100 })
  role_name: string;

  @Column('boolean', { default: true })
  role_active: Boolean;

  @OneToMany(() => Permissions, (permissions) => permissions)
  permissions: Permissions[];

  @OneToMany(() => Users, (users) => users.role)
  users: Users[];
}
