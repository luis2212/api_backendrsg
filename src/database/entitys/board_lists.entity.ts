
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, OneToMany, JoinColumn} from "typeorm";
import { Board } from "./board.entity";
import { Scrum_card } from "./scrum_card.entity";

@Entity()
export class Board_lists {

    @PrimaryGeneratedColumn({type:"int"})
    id_list: number;
    
    @ManyToOne(type=>Board, board=> board.lists)
    @JoinColumn({name : 'id_board'})
    id_board:Board;

    @Column({type:"varchar", length:20})
    list_tittle:string;

    @Column({type:"boolean"})
    archived_list: boolean;

    @OneToMany(()=>Scrum_card, scard=>scard.id_list, {eager:true})
    scards: Scrum_card[];
}