import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  JoinColumn,
  ManyToMany,
  ManyToOne,
} from 'typeorm';

import { Favorite_boards } from './favorite_boards.entity';

import { Role } from './role.entity';
import { Group } from './group.entity';
import { Board } from './board.entity';

import { Board_members } from './board_members.entity';

import { Board_moderator } from './board_moderator.entity';
import { Metting_tests } from './metting_tests.entity';
import { Ticket } from './ticket.entity';
import { Access_credentials } from './access_credentials.entity';
import { Scrum_card } from './scrum_card.entity';
import { Tasks } from './tasks.entity';
import { Card_comments } from './card_comments.entity';
import { Credentials_comments } from './credentials_comments.entity';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id_user: number;

  @Column({ length: 25, unique: true, nullable: false })
  username: string;

  @Column({ length: 64 })
  name: string;

  @Column({ length: 64 })
  lastname: string;

  @Column({ length: 254 , select: false})
  password: string;

  @Column({ type: 'text', nullable: true }) //Opcional
  photo?: string;

  @ManyToOne(() => Role, (role) => role.users, { eager: true }) //Aqui no es recomendable usar cascada (Porque el rol debe existir si o si previamente ya en base)
  @JoinColumn({ name: 'role_id' })
  role: Role;

  @OneToMany(() => Group, (group) => group.users)
  group: Group[];

  //Un usuario tiene N favoritos
  @OneToMany(() => Favorite_boards, (favorite_boards) => favorite_boards.users)
  favorite_boards: Favorite_boards[];

  @OneToMany(() => Board, (board) => board.users)
  board: Board[];

  @OneToMany(() => Board_members, (board_members) => board_members.users)
  board_members: Board_members[];

  @OneToMany(() => Board_moderator, (board_moderator) => board_moderator.users)
  board_moderator: Board_moderator[];

  @ManyToMany(() => Metting_tests, (metting_tests) => metting_tests.attendees)
  mettings: Metting_tests[];

  @ManyToMany(() => Ticket, (ticket) => ticket.attendees)
  tickets: Ticket[];

  @ManyToMany(
    () => Access_credentials,
    (access_credentials) => access_credentials.users_access,
  )
  credentials: Access_credentials[];

  @ManyToMany(() => Scrum_card, (scrum_card) => scrum_card.keppers)
  scards: Scrum_card[];

  @OneToMany(() => Tasks, (tasks) => tasks.user_attend, { eager: true })
  tasks: Tasks[];

  @OneToMany(
    () => Credentials_comments,
    (credentials_comments) => credentials_comments.users,
  )
  credentials_comments: Credentials_comments[];

  @OneToMany(() => Card_comments, (card_comments) => card_comments.users)
  card_comments: Card_comments[];
}
