import { Entity, Column, PrimaryGeneratedColumn ,OneToOne, JoinColumn, ManyToOne, Generated} from 'typeorm';


import {Data_group } from './data_group.entity';
import { Users} from './users.entity';

@Entity()
export class Group {

  @Column({unique:true})
  @Generated('increment')
  group_id: number;

    @ManyToOne( () => Data_group , (data_group) => data_group.group, {primary: true, eager:true})
    @JoinColumn({name: 'datagroup_id'})  
    data_group: Data_group;


    @ManyToOne( () => Users , (user) => user.group, {primary:true, eager:true})
    @JoinColumn({name: 'id_user'})  
    users: Users;



}