
import { Entity, Column, PrimaryColumn, ManyToOne, JoinColumn, Generated} from 'typeorm';

import {Role} from './role.entity';
import { Privileges } from './privileges.entity';

@Entity()
export class Permissions{

  @Column({unique:true})
  @Generated('increment')
  perm_id: number;
  

  @PrimaryColumn()
  @ManyToOne(() =>  Role , role => role.permissions, {eager : true})
  @JoinColumn({name : 'role_id'})
  role_id: number;
  

  @PrimaryColumn()
  @ManyToOne(() =>  Privileges , privileges => privileges.permissions, {eager : true})
  @JoinColumn({name : 'privi_id'})
  privi_id: number;
  
  
  @Column("boolean",{default: true})
  perm_active: Boolean;


}