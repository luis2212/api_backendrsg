import { Entity, Column, JoinColumn, ManyToOne, Generated, PrimaryColumn} from 'typeorm';


import { Users } from './users.entity';
import { Board } from './board.entity';

@Entity()
export class Board_members{

  @Column({unique:true})
  @Generated('increment')
  members_id: number;


  //Se usa una llave primaria compuesta para no repetir usuarios en un mismo proyecto

 
  @ManyToOne(() => Board , board => board.board_members , {primary:true, eager : true})
  @JoinColumn({name : 'id_board'})
  board: number;


  @ManyToOne(() => Users , users => users.board_members, {primary:true, eager : true})
  @JoinColumn({name : 'id_user'})
  users: number;


}