import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  OneToMany,
} from 'typeorm';

import { Privileges } from './privileges.entity';

@Entity()
export class Privilege_groups {
  @PrimaryGeneratedColumn()
  privig_id: number;

  @Column('character varying', { length: 25, unique: true, nullable: false })
  privig_name: string;

  @Column({nullable: true})
  privig_position: number;

  @OneToMany(() => Privileges, (privileges) => privileges.privilege_groups)
  privileges: Privileges[];
}
