import { Entity, Column, PrimaryGeneratedColumn ,OneToOne, OneToMany} from 'typeorm';

import { Group } from './group.entity';
import {Board} from './board.entity'



@Entity()
export class Data_group {

  @PrimaryGeneratedColumn()
  datagroup_id: number;

  @Column("varchar",{unique:true ,length:35})
  datagroup_name: string;

  @Column("text") //Opcional y no necesario
  datagroup_photo: string;


  @OneToMany(() => Group, (group) => group.data_group)   
  group:Group[];


  @OneToMany(() => Board, (board) => board.group)   
  board:Board[];
  
}