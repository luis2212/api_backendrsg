
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, JoinColumn} from "typeorm";
import { Board } from "./board.entity";
import { Scrum_card } from "./scrum_card.entity";

@Entity()
export class Labels {

    @PrimaryGeneratedColumn({type:"int"})
    id_label: number;

    @ManyToOne(type=>Board, board=> board.labels,{eager:true})
    @JoinColumn({name : 'id_board'})
    id_board:Board;

    @Column({type:"varchar", length:15})
    color:string;

    @Column({type:"varchar", length:25})
    label_tittle: string; 

    @ManyToMany(()=>Scrum_card, scard => scard.labels, {eager:true})
    scards: Scrum_card[];

}