
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, OneToOne, JoinColumn} from "typeorm";
import { Access_credentials } from "./access_credentials.entity";
import { Users } from "./users.entity";


@Entity()
export class Credentials_comments {

    @PrimaryGeneratedColumn({type:"int"})
    id_comment: number;

    @ManyToOne(type=>Access_credentials, access_credentials=> access_credentials.comments)
    @JoinColumn({name : 'id_credential'})
    id_credential:Access_credentials;

    @Column({type:"varchar", length:60})
    comment_content:string;


    @ManyToOne( () => Users , (users) =>  users.credentials_comments, {eager:true})
    @JoinColumn({name:'id_user'})  
    users: Users;

    
}