import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { Users } from './users.entity';
import { Data_group } from './data_group.entity';
import { Favorite_boards } from './favorite_boards.entity';
import { Board_members } from './board_members.entity';
import { Board_moderator } from './board_moderator.entity';
import { Objetive } from './objetive.entity';
import { Requerements } from './requerements.entity';
import { Scope } from './scope.entity';
import { Metting_tests } from './metting_tests.entity';
import { Ticket } from './ticket.entity';
import { Board_lists } from './board_lists.entity';
import { Access_credentials } from './access_credentials.entity';
import { Labels } from './labels.entity';

@Entity()
export class Board {
  @PrimaryGeneratedColumn()
  id_board: number;

  @Column('varchar', { length: 35, nullable: false })
  board_name: string;

  @ManyToOne(() => Users, (users) => users.board, { eager: true })
  @JoinColumn({ name: 'project_manager' })
  users: Users;


  @ManyToOne(() => Data_group, (group) => group.board, { eager: true })
  @JoinColumn({ name: 'team' })
  group: Data_group;


  @Column('date')
  start_date: Date;

  @Column('date')
  end_date: Date;

  @Column('boolean', { default: false }) //Se finalizo el proyecto
  board_finish: Boolean;

  @Column('text')
  description_board: String;

  @Column('boolean', { default: false })
  archived_board: Boolean;

  @OneToMany(() => Favorite_boards, (favorite_boards) => favorite_boards.Board)
  favorite_boards: Favorite_boards[];

  @OneToMany(() => Board_members, (board_members) => board_members.board)
  board_members: Board_members[];

  //-
  @OneToMany(() => Board_moderator, (board_moderator) => board_moderator.board)
  board_moderator: Board_moderator[];

  @OneToMany(() => Objetive, (objetive) => objetive.board)
  objetive: Objetive[];

  @OneToMany(() => Requerements, (requerements) => requerements.board)
  requerements: Requerements[];

  @OneToMany(() => Scope, (scope) => scope.board)
  scope: Scope[];

  @OneToMany(() => Metting_tests, (metting_test) => metting_test.board)
  metting_test: Metting_tests[];

  @OneToMany(() => Ticket, tickets => tickets.id_ticket)
  tickets: Ticket[];

  @OneToMany(() => Board_lists, blist => blist.id_board)
  lists: Board_lists[];

  @OneToMany(() => Access_credentials, access_credentials => access_credentials.id_board)
  credentials: Access_credentials[];

  @OneToMany(() => Labels, labels => labels.id_board)
  labels: Labels[];
}
