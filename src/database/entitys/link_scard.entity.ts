
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, JoinColumn} from "typeorm";
import { Scrum_card } from "./scrum_card.entity";

@Entity()
export class Link_scard {

    @PrimaryGeneratedColumn({type:"int"})
    id_link: number;

    @ManyToOne(type=>Scrum_card, scard=> scard.links)
    @JoinColumn({name : 'id_scard'})
    id_scard:Scrum_card;

    @Column({type:"text"})
    link_content:string;

    @Column({type:"varchar", length:25})
    link_name: string; 

   
}