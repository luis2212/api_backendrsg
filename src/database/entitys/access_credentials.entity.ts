import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  ManyToOne,
  OneToMany,
  JoinColumn,
  JoinTable,
} from 'typeorm';
import { Board } from './board.entity';
import { Credentials_comments } from './credentials_comments.entity';
import { Users } from './users.entity';

@Entity()
export class Access_credentials {
  @PrimaryGeneratedColumn({ type: 'int' })
  id_credential: number;


  @ManyToOne((type) => Board, (board) => board.credentials, { eager: true })
  @JoinColumn({name:'id_board'})
  id_board: Board;


  @Column({ type: 'varchar', length: 80 })
  credential_description: string;

  @Column({ type: 'text' })
  credential_links: string;

  @Column({ type: 'text' })
  credential_filename: string;

  @Column({ type: 'text' })
  credential_path: string;

  @OneToMany(
    (type) => Credentials_comments,
    (comments) => comments.id_credential,{
      eager:true
    }
  )
  comments: Credentials_comments[];
  
  
  @ManyToMany(()=> Users, user => user.credentials,{ eager:true })
  @JoinTable(
    { name: "credetials_users", 
      joinColumn:{
        name:'id_credential'
      },
      inverseJoinColumn:
      {
        name:'id_user'
      }  
    })
  users_access: Users[];
  
}
