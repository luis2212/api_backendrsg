import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  JoinColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

import { Permissions } from './permissions.entity';

import { Privilege_groups } from './privilege_groups.entity';

@Entity()
export class Privileges {
  @PrimaryGeneratedColumn()
  privi_id: number;

  @Column('character varying', { length: 20, unique: true, nullable: false })
  privi_code: string;

  @Column('character varying', { length: 100 })
  privi_name: string;

  @Column('boolean', { default: true })
  privi_active: Boolean;

  @ManyToOne(
    () => Privilege_groups,
    (privilege_groups) => privilege_groups.privileges,
    { eager: true },
  )
  @JoinColumn({ name: 'privig_id' })
  privilege_groups: Privilege_groups;

  @OneToMany(() => Permissions, (permissions) => permissions)
  permissions: Permissions[];
}
