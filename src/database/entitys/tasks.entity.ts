import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Checklist } from './checklist.entity';
import { Users } from './users.entity';

@Entity()
export class Tasks {
  @PrimaryGeneratedColumn({ type: 'int' })
  id_task: number;

  @ManyToOne((type) => Checklist, (checklist) => checklist.tasks)
  @JoinColumn({name : 'id_checklist'})
  id_checklist: Checklist;

  @Column({ type: 'varchar', length: 50 })
  task_description: string;

  @Column({ type: 'boolean' })
  task_status: boolean;

  @Column({ type: 'date' })
  scheduled_data: Date;

  @Column({ type: 'timestamp' })
  start_date: string;

  @Column({ type: 'timestamp' })
  end_date: string;

  @Column({ type: 'time' })
  task_time: string;

  @Column({ type: 'text' })
  task_filename: string;

  @Column({ type: 'text' })
  task_link: string;

  @ManyToOne(() => Users, (users) => users.tasks)
  @JoinColumn({name : 'id_user'})
  user_attend: Users;

}
