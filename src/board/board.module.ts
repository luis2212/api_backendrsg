import { Module } from '@nestjs/common';
import { BoardController } from './board.controller';
import { BoardService } from './board.service';



import { TypeOrmModule } from '@nestjs/typeorm';
import {Board} from '../database/entitys/board.entity'
import {Data_group} from '../database/entitys/data_group.entity'
import {Users} from '../database/entitys/users.entity'



@Module({
  imports:[TypeOrmModule.forFeature([Board, Data_group, Users])],
  controllers: [BoardController],
  providers: [BoardService]
})
export class BoardModule {}
