import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';


import {BoardService}  from './board.service'

@Controller('board')
export class BoardController {

    constructor ( private boardService : BoardService) {}

    @Get()
    async get()
    {
        return await this.boardService.getAll();
    }



    @Get('/:id_board')
    getbyID(@Param('id_board') id_board)
    {
       return this.boardService.findOne(id_board);
    }


    @Post()
    async create(@Body() board)
    {
        return await this.boardService.create(board);
    }



    @Put('/:id_board')
    async update(@Param('id_board') id_board, @Body() board){

        return await this.boardService.update(id_board, board)

    }



    @Delete('/:id_board')
    async delete(@Param('id_board') id_board)
    {
        return await this.boardService.delete(id_board)
    }



}
