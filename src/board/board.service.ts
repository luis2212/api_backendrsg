import { Injectable } from '@nestjs/common';


//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';

//Entidades
import { Board } from '../database/entitys/board.entity';
import {Data_group} from '../database/entitys/data_group.entity'
import {Users} from '../database/entitys/users.entity'


@Injectable()
export class BoardService {


    
    constructor(@InjectRepository(Board) private repoBoard:Repository<Board>,
    @InjectRepository(Data_group) private repoDatagroup:Repository<Data_group>,
    @InjectRepository(Users) private repoUser:Repository<Users>){}


    async getAll()
    {
       try
        {
          return await this.repoBoard.find({order : {'id_board': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }



    async findOne(id_board)
    {
        try{
        const foundboard = await this.repoBoard.findOne(id_board)
        
        if(!foundboard) return {Message: 'Board not found', Status: '404'}
        
        return {Message: foundboard, Status:'201'}
        

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }

    }



    /*

    Json que recibe:

        { 
            "board_name": "Desarrollo SDK" , 
            "start_date": "2021-10-13",
            "end_date": "2021-12-01",
            "board_finish": false,
            "description_board": "Desarrollo SDK para Bimbo" ,
            "archived_board": false, 
            "project_manager": 1, 
            "team":2 
        }

    */



    async create(board)
    {
        try {  
            
            //Extraemos info
            const newBoard = new Board

            newBoard.board_name = board.board_name
            newBoard.start_date = board.start_date
            newBoard.end_date    = board.end_date
            newBoard.board_finish = board.board_finish
            newBoard.description_board = board. description_board
            newBoard.archived_board  = board.archived_board
           
            //Buscamos el Team o "Data Group"

            const findDataGroup = await this.repoDatagroup.findOne(board.team)
            if(!findDataGroup) return {Message: 'Team not found', Status: '404'}
            //Asignamos Team
            newBoard.group = findDataGroup


            //Buscamos User
            const findUser = await this.repoUser.findOne(board.project_manager)
            if(!findUser) return {Message: 'Manager not found', Status: '404'}
            //Asignamos User
            newBoard.users = findUser
            

            const saveBoard= await this.repoBoard.save(newBoard);
            return {Message: saveBoard , Status:'201'}
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }



   

    async update(id_board, board){

        try
        {
            const findBoard  = await this.repoBoard.findOne(id_board);
            
            if(!findBoard) return {Message: 'Board not found', Status: '404'}

            
            //Extraemos info
            const newBoard = new Board

            newBoard.board_name = board.board_name
            newBoard.start_date = board.start_date
            newBoard.end_date    = board.end_date
            newBoard.board_finish = board.board_finish
            newBoard.description_board = board. description_board
            newBoard.archived_board  = board.archived_board

            //Buscamos el Team o "Data Group"

            const findDataGroup = await this.repoDatagroup.findOne(board.team)
            if(!findDataGroup) return {Message: 'Team not found', Status: '404'}
            //Asignamos Team
            newBoard.group = findDataGroup


            //Buscamos User
            const findUser = await this.repoUser.findOne(board.project_manager)
            if(!findUser) return {Message: 'Manager not found', Status: '404'}
            //Asignamos User
            newBoard.users = findUser

            this.repoBoard.merge(findBoard, newBoard);

            //Objeto Actualizado
            const saveRole = await this.repoBoard.save(findBoard)
            return {'Message': saveRole , Status:'201'};

        
        } catch (error) {
         
            return {Message: 'Error', Status: '400'}
        }

    }





    async delete(id_board)
    {
        try
        {
            const deleteRole = await this.repoBoard.delete(id_board);

            if(deleteRole.affected == 0) return {Message: 'Board not found', Status: '404'}
        
            return {'Message': 'Board deleted', Status:'201'};
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }







}
