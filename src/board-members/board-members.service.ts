import { Injectable } from '@nestjs/common';


//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';

//Entidades
import { Board_members } from '../database/entitys/board_members.entity';
import { Board } from '../database/entitys/board.entity';
import {Users} from '../database/entitys/users.entity'



@Injectable()
export class BoardMembersService {

    constructor(@InjectRepository(Board) private repoBoard:Repository<Board>,
    @InjectRepository(Board_members) private repoMember:Repository<Board_members>,
    @InjectRepository(Users) private repoUser:Repository<Users>){}



    async getAll()
    {
       try
        {
          return await this.repoMember.find({order : {'members_id': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }




    //Todos los integrantes de un Boars
    async findBoardsUser(id_board)
    {
        try{

        //Buscamos Board
        const findBoard = await this.repoBoard.findOne(id_board)
        if(!findBoard) return {Message: 'Board not found', Status: '404'}

        const foundFavorites = await this.repoMember.find({select:['users'], where:{board:findBoard}})
       
        return {Message: foundFavorites, Status:'201'}
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }

    }









    /*
            { 
                "id_board":1,
                "id_user": 3
            }
    */


    async create(Member)
    {
        try {  
            
            //Creamos Variable para guardar el Usuario y Board
            const newMember = new Board_members

            //Buscamos el Board
            const findBoard = await this.repoBoard.findOne(Member.id_board)
            if(!findBoard) return {Message: 'Board not found', Status: '404'}
            
            //Asignamos Bord
            newMember.board = findBoard.id_board

            //Buscamos User
            const findUser = await this.repoUser.findOne(Member.id_user)
            if(!findUser) return {Message: 'User not found', Status: '404'}
            //Asignamos User
            newMember.users = findUser.id_user
            
            const saveFavorite= await this.repoMember.save(newMember);
            return {Message: saveFavorite , Status:'201'}

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }







    async delete(Member)
    {
        try
        {
           
             //Creamos Variable para guardar el Usuario y Board
             const newMember = new Board_members

             //Buscamos el Board
             const findBoard = await this.repoBoard.findOne(Member.id_board)
             if(!findBoard) return {Message: 'Board not found', Status: '404'}
             
             //Asignamos Bord
             newMember.board = findBoard.id_board
 
             //Buscamos User
             const findUser = await this.repoUser.findOne(Member.id_user)
             if(!findUser) return {Message: 'User not found', Status: '404'}
             //Asignamos User
             newMember.users = findUser.id_user
           
           
            const deleteMember = await this.repoMember.delete(newMember)

           
            if(deleteMember.affected == 0) return {Message: 'Member not found', Status: '404'}
        
           
            return {'Message': 'Member deleted', Status:'201'};
        
            
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }



}
