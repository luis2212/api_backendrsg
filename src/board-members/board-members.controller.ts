import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';


import { BoardMembersService} from './board-members.service'

@Controller('board_members')
export class BoardMembersController {

    
    constructor ( private membersService : BoardMembersService) {}


    @Get()
    async get()
    {
        return await this.membersService.getAll();
    }


    //Se recibe el Id del Board
    @Get('/:id_board')
    getbyID(@Param('id_board') id_board)
    {
       return this.membersService.findBoardsUser(id_board);
    }



    

    @Post()
    async create(@Body() Member)
    {
        return await this.membersService.create(Member);
    }




    // No existe Actualizacón!!!!!!!!

            //Un usuario es miembro de un equipo o no es miembro.
    
                //Si no es: elimina ese registro
    






     //Para eliminar 

            //Se deve recibir un Body con IdUser y IdBoard
    
                
                  /*
                  Json:
                    { 
                        "id_board":3,
                        "id_user": 3 
                    }
                  */
    
    @Delete()
    async delete(@Body() Member)
    {
      return await this.membersService.delete(Member)
    }


}
