import { Module } from '@nestjs/common';
import { BoardMembersController } from './board-members.controller';
import { BoardMembersService } from './board-members.service';


import { TypeOrmModule } from '@nestjs/typeorm';
import { Board_members } from '../database/entitys/board_members.entity';
import {Board} from '../database/entitys/board.entity'
import {Users} from '../database/entitys/users.entity'


@Module({
  imports:[TypeOrmModule.forFeature([Board, Users,Board_members])],
  controllers: [BoardMembersController],
  providers: [BoardMembersService]
})

export class BoardMembersModule {}
