import { Module } from '@nestjs/common';
import { BoardModeratorController } from './board-moderator.controller';
import { BoardModeratorService } from './board-moderator.service';


import { TypeOrmModule } from '@nestjs/typeorm';
import { Board_moderator } from '../database/entitys/board_moderator.entity';
import {Board} from '../database/entitys/board.entity'
import {Users} from '../database/entitys/users.entity'

@Module({
  imports:[TypeOrmModule.forFeature([Board, Users,Board_moderator])],
  controllers: [BoardModeratorController],
  providers: [BoardModeratorService]
})
export class BoardModeratorModule {}
