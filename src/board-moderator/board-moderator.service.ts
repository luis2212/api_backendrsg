import { Injectable } from '@nestjs/common';


//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';

//Entidades
import { Board_moderator } from '../database/entitys/board_moderator.entity';
import { Board } from '../database/entitys/board.entity';
import {Users} from '../database/entitys/users.entity'



@Injectable()
export class BoardModeratorService {

    constructor(@InjectRepository(Board) private repoBoard:Repository<Board>,
    @InjectRepository(Board_moderator) private repoModerator:Repository<Board_moderator>,
    @InjectRepository(Users) private repoUser:Repository<Users>){}



    async getAll()
    {
       try
        {
          return await this.repoModerator.find({order : {'moderator_id': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }



     //Todos los Moderadores de un Boars
     async findBoardsModerator(id_board)
     {
         try{
 
         //Buscamos Board
         const findBoard = await this.repoBoard.findOne(id_board)
         if(!findBoard) return {Message: 'Board not found', Status: '404'}
 
         const foundFavorites = await this.repoModerator.find({select:['users'], where:{board:findBoard}})
        
         return {Message: foundFavorites, Status:'201'}
         
         } catch (error) {
             return {Message: 'Error', Status: '400'}
         }
 
     }









     
    /*
            { 
                "id_board":1,
                "id_user": 3
            }
    */


    async create(Moderator)
    {
        try {  
            
            //Creamos Variable para guardar el Usuario y Board
            const newModerator = new Board_moderator

            //Buscamos el Board
            const findBoard = await this.repoBoard.findOne(Moderator.id_board)
            if(!findBoard) return {Message: 'Board not found', Status: '404'}
            
            //Asignamos Bord
            newModerator.board = findBoard.id_board

            //Buscamos User
            const findUser = await this.repoUser.findOne(Moderator.id_user)
            if(!findUser) return {Message: 'User not found', Status: '404'}
            //Asignamos User
            newModerator.users = findUser.id_user
            
            const saveModerator= await this.repoModerator.save(newModerator);
            return {Message: saveModerator , Status:'201'}

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }





    async delete(Moderator)
    {
        try
        {
           
             //Creamos Variable para guardar el Usuario y Board
             const newModerator = new Board_moderator

             //Buscamos el Board
             const findBoard = await this.repoBoard.findOne(Moderator.id_board)
             if(!findBoard) return {Message: 'Board not found', Status: '404'}
             
             //Asignamos Bord
             newModerator.board = findBoard.id_board
 
             //Buscamos User
             const findUser = await this.repoUser.findOne(Moderator.id_user)
             if(!findUser) return {Message: 'User not found', Status: '404'}
             //Asignamos User
             newModerator.users = findUser.id_user
           
    
            const deleteModerator = await this.repoModerator.delete(newModerator)
           
            if(deleteModerator.affected == 0) return {Message: 'Moderator not found', Status: '404'}
        
            return {'Message': 'Moderator Deleted', Status:'201'};
            
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }

}
