import { Test, TestingModule } from '@nestjs/testing';
import { BoardModeratorController } from './board-moderator.controller';

describe('BoardModeratorController', () => {
  let controller: BoardModeratorController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BoardModeratorController],
    }).compile();

    controller = module.get<BoardModeratorController>(BoardModeratorController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
