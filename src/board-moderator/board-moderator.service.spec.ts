import { Test, TestingModule } from '@nestjs/testing';
import { BoardModeratorService } from './board-moderator.service';

describe('BoardModeratorService', () => {
  let service: BoardModeratorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BoardModeratorService],
    }).compile();

    service = module.get<BoardModeratorService>(BoardModeratorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
