import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';


import { BoardModeratorService } from './board-moderator.service';

@Controller('board_moderator')
export class BoardModeratorController {

    
    constructor ( private moderatorService : BoardModeratorService) {}

    @Get()
    async get()
    {
        return await this.moderatorService.getAll();
    }



     //Se recibe el Id del Board
     @Get('/:id_board')
     getbyID(@Param('id_board') id_board)
     {
        return this.moderatorService.findBoardsModerator(id_board);
     }




     @Post()
    async create(@Body() Moderator)
    {
        return await this.moderatorService.create(Moderator);
    }





    // No existe Actualizacón!!!!!!!!

            //Un usuario es Moderador de un equipo o no es Moderador.    
                //Si no es: elimina ese registro
    



     //Para eliminar 
            //Se deve recibir un Body con IdUser y IdBoard
                
                  /*
                  Json:
                    { 
                        "id_board":3,
                        "id_user": 3 
                    }
                  */
    


    @Delete()
    async delete(@Body() Moderator)
    {
    return await this.moderatorService.delete(Moderator)
    }


    
}
