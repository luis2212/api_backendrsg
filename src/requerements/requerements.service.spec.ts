import { Test, TestingModule } from '@nestjs/testing';
import { RequerementsService } from './requerements.service';

describe('RequerementsService', () => {
  let service: RequerementsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RequerementsService],
    }).compile();

    service = module.get<RequerementsService>(RequerementsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
