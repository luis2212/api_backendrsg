import { Module } from '@nestjs/common';
import { RequerementsController } from './requerements.controller';
import { RequerementsService } from './requerements.service';



import { TypeOrmModule } from '@nestjs/typeorm';
import { Requerements} from '../database/entitys/requerements.entity';
import {Board} from '../database/entitys/board.entity'


@Module({
  imports:[TypeOrmModule.forFeature([Board, Requerements])],
  controllers: [RequerementsController],
  providers: [RequerementsService]
})
export class RequerementsModule {}
