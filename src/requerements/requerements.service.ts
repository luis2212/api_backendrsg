import { Injectable } from '@nestjs/common';



//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';

//Entidades
import { Requerements } from '../database/entitys/requerements.entity';
import {Board} from '../database/entitys/board.entity'



@Injectable()
export class RequerementsService {

    constructor(@InjectRepository(Board) private repoBoard:Repository<Board>,
    @InjectRepository(Requerements) private repoRequerements:Repository<Requerements>){}



    async getAll()
    {
       try
        {
          return await this.repoRequerements.find({order : {'id_request': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }



    async findOne(id_request)
    {
        try{
        const foundrole = await this.repoRequerements.findOne(id_request)
        if(!foundrole) return {Message: 'Request not found', Status: '404'}
        return {Message: foundrole, Status:'201'}

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }

    }



    //Todos los requerimeitnos de un Board
            // Ruta ->  http://localhost:3000/requerements/board/1
            async findRequerementBoard(id_board)
            {
                try{
                //Buscamos Board
                const findBoard = await this.repoBoard.findOne(id_board)
                if(!findBoard) return {Message: 'Board not found', Status: '404'}
        
                const foundFavorites = await this.repoRequerements.find({where:{board:findBoard}, order : {'id_request': 'ASC'}})
               
                return {Message: foundFavorites, Status:'201'}
                
                } catch (error) {
                    return {Message: 'Error', Status: '400'}
                }
        
            }




        /*
            JSON:
            { 
                "request_description": "Citas con tigo" , 
                "request_status": true,
                "id_board" : 3
            }
        */

    async create(requerement)
    {
                try {  
        
                    //Desestrucutramos
                    const newRequerement = new Requerements
                    newRequerement.request_description = requerement.request_description
                    newRequerement.request_status = requerement.request_status
                    
        
                    //Buscamos Board
                    const findBoard = await this.repoBoard.findOne(requerement.id_board)
                    if(!findBoard) return {Message: 'Board not found', Status: '404'}
                    newRequerement.board = findBoard
        
                    const saveRequerement= await this.repoRequerements.save(newRequerement);
                
                    return {Message: saveRequerement , Status:'201'}
                } catch (error) {
                    return {Message: 'Error', Status: '400'}
                }
    }

    



    async update(id_request, requerement){

        try
        {
            const findRequerement  = await this.repoRequerements.findOne(id_request);            
            if(!findRequerement) return {Message: 'Requerement not found', Status: '404'}

            //Desestrucutramos
            const newRequerement = new Requerements
            newRequerement.request_description = requerement.request_description
            newRequerement.request_status = requerement.request_status
            

            //Buscamos Board
            const findBoard = await this.repoBoard.findOne(requerement.id_board)
            if(!findBoard) return {Message: 'Board not found', Status: '404'}
            newRequerement.board = findBoard


            this.repoRequerements.merge(findRequerement , newRequerement);

            //Objeto Actualizado
            const saveObjetive = await this.repoRequerements.save(findRequerement)
            return {'Message': saveObjetive , Status:'201'};

        
        } catch (error) {
         
            return {Message: 'Error', Status: '400'}
        }

    }


    


    async delete(id_request)
    {
        try
        {
            const deleteRequets = await this.repoRequerements.delete(id_request);

            if(deleteRequets.affected == 0) return {Message: 'Requerement not found', Status: '404'}
        
            return {'Message': 'Objetive deleted', Status:'201'};
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }







}
