import { Test, TestingModule } from '@nestjs/testing';
import { RequerementsController } from './requerements.controller';

describe('RequerementsController', () => {
  let controller: RequerementsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RequerementsController],
    }).compile();

    controller = module.get<RequerementsController>(RequerementsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
