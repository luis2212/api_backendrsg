import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';

import { RequerementsService } from './requerements.service';

@Controller('requerements')
export class RequerementsController {

    constructor ( private requerementsService : RequerementsService) {}


    @Get()
    async get()
    {
        return await this.requerementsService.getAll();
    }


    @Get('/:id_request')
    getbyID(@Param('id_request') id_request)
    {
       return this.requerementsService.findOne(id_request);
    }



     //Se recibe el ID del Board   "Ver todos los requerimientos de un proyecto"
     @Get('board/:id_board')
     getbyBoard(@Param('id_board') id_board)
     {
        return this.requerementsService.findRequerementBoard(id_board);
     }




     @Post()
    async create(@Body() requerement)
    {
        return await this.requerementsService.create(requerement);
    }





    @Put('/:id_request')
    async update(@Param('id_request') id_request, @Body() requerement){

        return await this.requerementsService.update(id_request, requerement)

    }



    @Delete('/:id_request')
    async delete(@Param('id_request') id_request)
    {
        return await this.requerementsService.delete(id_request)
    }


}
