import { Module } from '@nestjs/common';
import { FavoriteBoardsController } from './favorite-boards.controller';
import { FavoriteBoardsService } from './favorite-boards.service';


import { TypeOrmModule } from '@nestjs/typeorm';
import {Board} from '../database/entitys/board.entity'
import {Users} from '../database/entitys/users.entity'
import { Favorite_boards } from '../database/entitys/favorite_boards.entity';


@Module({
  imports:[TypeOrmModule.forFeature([Board, Users,Favorite_boards])],
  controllers: [FavoriteBoardsController],
  providers: [FavoriteBoardsService]
})
export class FavoriteBoardsModule {}
