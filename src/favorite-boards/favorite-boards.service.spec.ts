import { Test, TestingModule } from '@nestjs/testing';
import { FavoriteBoardsService } from './favorite-boards.service';

describe('FavoriteBoardsService', () => {
  let service: FavoriteBoardsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FavoriteBoardsService],
    }).compile();

    service = module.get<FavoriteBoardsService>(FavoriteBoardsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
