import { Test, TestingModule } from '@nestjs/testing';
import { FavoriteBoardsController } from './favorite-boards.controller';

describe('FavoriteBoardsController', () => {
  let controller: FavoriteBoardsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FavoriteBoardsController],
    }).compile();

    controller = module.get<FavoriteBoardsController>(FavoriteBoardsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
