import { Injectable } from '@nestjs/common';

//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';

//Entidades
import { Favorite_boards } from '../database/entitys/favorite_boards.entity';
import { Board } from '../database/entitys/board.entity';
import {Users} from '../database/entitys/users.entity'


@Injectable()
export class FavoriteBoardsService {

    constructor(@InjectRepository(Board) private repoBoard:Repository<Board>,
    @InjectRepository(Favorite_boards) private repoFavorite:Repository<Favorite_boards>,
    @InjectRepository(Users) private repoUser:Repository<Users>){}


    async getAll()
    {
       try
        {
          return await this.repoFavorite.find({order : {'id_favorite': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }





    //Todos los Board Favorites de un usuario
    async findUser(id_user)
    {
        try{


        //Buscamos User
        const findUser = await this.repoUser.findOne(id_user)
        if(!findUser) return {Message: 'Users not found', Status: '404'}

    
        const foundFavorites = await this.repoFavorite.find({select:['Board'],where:{users:findUser}})
       
    
        return {Message: foundFavorites, Status:'201'}
        

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }

    }






    /*
        Json que recibe:   
            { 
                "id_user": 3, 
                "id_board":3
            }

    */


    async create(favorite)
    {
        try {  
            
            //Creamos Variable para guardar el Usuario y Board
            const newfavorite = new Favorite_boards

            //Buscamos el Board
            const findBoard = await this.repoBoard.findOne(favorite.id_board)
            if(!findBoard) return {Message: 'Board not found', Status: '404'}
            
            //Asignamos Bord
            newfavorite.Board = findBoard

            //Buscamos User
            const findUser = await this.repoUser.findOne(favorite.id_user)
            if(!findUser) return {Message: 'User not found', Status: '404'}
            //Asignamos User
            newfavorite.users = findUser
            
            const saveFavorite= await this.repoFavorite.save(newfavorite);
            return {Message: saveFavorite , Status:'201'}

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }







    async delete(favorite)
    {
        try
        {
           
            //Creamos Variable para guardar el Usuario y Board
            const newfavorite = new Favorite_boards

            //Buscamos el Board
            const findBoard = await this.repoBoard.findOne(favorite.id_board)
            if(!findBoard) return {Message: 'Board not found', Status: '404'}
            
            //Asignamos Bord
            newfavorite.Board = findBoard

            //Buscamos User
            const findUser = await this.repoUser.findOne(favorite.id_user)
            if(!findUser) return {Message: 'User not found', Status: '404'}
            //Asignamos User
            newfavorite.users = findUser
           
           
            const deleteMember = await this.repoFavorite.delete(newfavorite)

           
            if(deleteMember.affected == 0) return {Message: 'Favorite not found', Status: '404'}
        
           
            return {'Message': 'Favorite deleted', Status:'201'};
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }


}
