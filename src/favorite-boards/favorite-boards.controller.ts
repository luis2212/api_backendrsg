import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';


import { FavoriteBoardsService } from './favorite-boards.service';

@Controller('favorite_boards')
export class FavoriteBoardsController {

    constructor ( private favoriteService : FavoriteBoardsService) {}

    
    @Get()
    async get()
    {
        return await this.favoriteService.getAll();
    }





    //Se recibe el Id del Usuario
    @Get('/:id_user')
    getbyID(@Param('id_user') id_user)
    {
       return this.favoriteService.findUser(id_user);
    }





    @Post()
    async create(@Body() favorite)
    {
        return await this.favoriteService.create(favorite);
    }







      // No existe Actualizacón!!!!!!!!

            //Un usuario tienen o no tiene un Board Favorite
    
                //Si no es: elimina ese registro
    







     //Para eliminar 

            //Se deve recibir un Body con IdUser y IdBoard
    
                
                  /*
                  Json:
                    { 
                        "id_user": 3, 
                        "id_board":3
                    }
                     */

    @Delete()
    async delete(@Body() favorite)
    {
        return await this.favoriteService.delete(favorite)
     }
}
