import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { Attachments } from 'src/database/entitys/attachments.entity';
import { AttachmentsService } from './attachments.service';

@Controller('attachments')
export class AttachmentsController {
  constructor(
    private attachmentService: AttachmentsService,
  ){}

  @Get()
  async getAllAttachments(): Promise<Attachments[]>{
    return this.attachmentService.getAllAttachments();
  }

  @Get('/:id')
  async getOneAttachments(@Param('id') id: number): Promise<Attachments>{
    return this.attachmentService.getOneAttachment(id);
  }

  @Post()
  async createAttachment(@Body() attachments){
    return this.attachmentService.createAttachment(attachments);
  }

  @Delete('/:id')
  async deleteAttachment(@Param('id') id: number){
    return this.attachmentService.deleteAttachment(id);
  }

  @Put('/:id')
  async updateAttachment(@Param('id') id: number, @Body() attachments: Attachments){
    return this.attachmentService.updateAttachment(id, attachments);
  }
}
