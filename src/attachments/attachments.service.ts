import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Attachments } from 'src/database/entitys/attachments.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AttachmentsService {
  constructor(
    @InjectRepository(Attachments)
    private attachmentsRepository: Repository<Attachments>,
  ) {}

  async getAllAttachments(): Promise<Attachments[]> {
    return this.attachmentsRepository.find();
  }

  async getOneAttachment(id_attachment: number): Promise<Attachments> {
    return this.attachmentsRepository.findOne(id_attachment);
  }

  async createAttachment(attachment: Attachments) {
    try {
      const newAttachment = await this.attachmentsRepository.create(attachment);
      const saveAttachment = await this.attachmentsRepository.save(
        newAttachment,
      );

      return {
        Message: 'Attachment saved successfully',
        Attachment: saveAttachment,
        Status: '201',
      };
    } catch (err) {
      return {
        Message: 'Error creating Attachment',
        Error: err.message,
        Status: 400,
      };
    }
  }

  async deleteAttachment(id_attachment: number) {
    try {
      const deleteAttachment = await this.attachmentsRepository.delete(
        id_attachment,
      );

      return { Message: 'Attachment deleted successfully', Status: 201 };
    } catch (err) {
      return {
        Message: 'Error deleting attachment',
        Error: err.message,
        Status: 400,
      };
    }
  }

  async updateAttachment(id_attachment: number, attachment: Attachments) {
    try {
      const foundAttachment = await this.attachmentsRepository.findOne(
        id_attachment,
      );
      !foundAttachment
        ? { Message: 'Attachment not found', Status: 404 }
        : await this.attachmentsRepository.merge(foundAttachment, attachment);

      const saveAttachment = await this.attachmentsRepository.save(
        foundAttachment,
      );

      return { Message: 'Attachment updated successfully', Status: 201 };
    } catch (err) {
      return {
        Message: 'Error updated attachment',
        Error: err.message,
        Status: 400,
      };
    }
  }
}
