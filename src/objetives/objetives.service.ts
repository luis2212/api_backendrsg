import { Injectable } from '@nestjs/common';


//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';

//Entidades
import { Objetive } from '../database/entitys/objetive.entity';
import {Board} from '../database/entitys/board.entity'


@Injectable()
export class ObjetivesService {

    constructor(@InjectRepository(Board) private repoBoard:Repository<Board>,
    @InjectRepository(Objetive) private repoObjetive:Repository<Objetive>){}


    async getAll()
    {
       try
        {
          return await this.repoObjetive.find({order : {'id_objetive': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }



    async findOne(id_objetive)
    {
        try{
        const foundrole = await this.repoObjetive.findOne(id_objetive)
        if(!foundrole) return {Message: 'Objetive not found', Status: '404'}
        return {Message: foundrole, Status:'201'}

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }

    }



    //Todos los Objetives de un Board
            // Ruta ->  http://localhost:3000/objetives/board/1
    async findObjetivesBoard(id_board)
    {
        try{
        //Buscamos Board
        const findBoard = await this.repoBoard.findOne(id_board)
        if(!findBoard) return {Message: 'Board not found', Status: '404'}

        const foundFavorites = await this.repoObjetive.find({where:{board:findBoard}, order : {'id_objetive': 'ASC'}})
       
        return {Message: foundFavorites, Status:'201'}
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }

    }




    /*
        { 
            "objetive_tittle": "Crear Fecha" , 
            "target_status": false,
            "id_board" : 1
        }

    */


    async create(objetive)
    {
        try {  

            //Desestrucutramos
            const newObjetive = new Objetive
            newObjetive.objetive_tittle = objetive.objetive_tittle
            newObjetive.target_status = objetive.target_status
            

            //Buscamos Board
            const findBoard = await this.repoBoard.findOne(objetive.id_board)
            if(!findBoard) return {Message: 'Board not found', Status: '404'}
            newObjetive.board = findBoard

            const saveObjetive= await this.repoObjetive.save(newObjetive);
        
            return {Message: saveObjetive , Status:'201'}
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }




    async update(id_objetive, objetive){

        try
        {
            const findObjetive  = await this.repoObjetive.findOne(id_objetive);            
            if(!findObjetive) return {Message: 'Objetive not found', Status: '404'}

            //Desestrucutramos
            const newObjetive = new Objetive
            newObjetive.objetive_tittle = objetive.objetive_tittle
            newObjetive.target_status = objetive.target_status
            

            //Buscamos Board
            const findBoard = await this.repoBoard.findOne(objetive.id_board)
            if(!findBoard) return {Message: 'Board not found', Status: '404'}
            newObjetive.board = findBoard


            this.repoObjetive.merge(findObjetive,  newObjetive);

            //Objeto Actualizado
            const saveObjetive = await this.repoObjetive.save(findObjetive)
            return {'Message': saveObjetive , Status:'201'};

        
        } catch (error) {
         
            return {Message: 'Error', Status: '400'}
        }

    }




    async delete(id_objetive)
    {
        try
        {
            const deleteObjetive = await this.repoObjetive.delete(id_objetive);

            if(deleteObjetive.affected == 0) return {Message: 'Objetive not found', Status: '404'}
        
            return {'Message': 'Objetive deleted', Status:'201'};
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }


}
