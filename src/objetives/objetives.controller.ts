import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';

import { ObjetivesService } from './objetives.service';

@Controller('objetives')
export class ObjetivesController {

    constructor ( private objetiveService : ObjetivesService) {}


    @Get()
    async get()
    {
        return await this.objetiveService.getAll();
    }


  
    @Get('/:id_objetive')
    getbyID(@Param('id_objetive') id_objetive)
    {
       return this.objetiveService.findOne(id_objetive);
    }






    //Se recibe el ID del Board   "Ver todos los objetivos de un proyecto"
    @Get('board/:id_board')
    getbyBoard(@Param('id_board') id_board)
    {
       return this.objetiveService.findObjetivesBoard(id_board);
    }






    @Post()
    async create(@Body() objetive)
    {
        return await this.objetiveService.create(objetive);
    }



    @Put('/:id_objetive')
    async update(@Param('id_objetive') id_objetive, @Body() objetive){

        return await this.objetiveService.update(id_objetive, objetive)

    }




    
    @Delete('/:id_objetive')
    async delete(@Param('id_objetive') id_objetive)
    {
        return await this.objetiveService.delete(id_objetive)
    }








}
