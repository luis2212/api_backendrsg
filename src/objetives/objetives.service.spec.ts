import { Test, TestingModule } from '@nestjs/testing';
import { ObjetivesService } from './objetives.service';

describe('ObjetivesService', () => {
  let service: ObjetivesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ObjetivesService],
    }).compile();

    service = module.get<ObjetivesService>(ObjetivesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
