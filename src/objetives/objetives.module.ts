import { Module } from '@nestjs/common';
import { ObjetivesController } from './objetives.controller';
import { ObjetivesService } from './objetives.service';


import { TypeOrmModule } from '@nestjs/typeorm';
import { Objetive } from '../database/entitys/objetive.entity';
import {Board} from '../database/entitys/board.entity'


@Module({
  imports:[TypeOrmModule.forFeature([Board, Objetive])],
  controllers: [ObjetivesController],
  providers: [ObjetivesService]
})
export class ObjetivesModule {}
