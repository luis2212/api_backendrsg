import { Test, TestingModule } from '@nestjs/testing';
import { ObjetivesController } from './objetives.controller';

describe('ObjetivesController', () => {
  let controller: ObjetivesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ObjetivesController],
    }).compile();

    controller = module.get<ObjetivesController>(ObjetivesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
