import { Injectable } from '@nestjs/common';


//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';

//Entidades
import { Permissions } from '../database/entitys/permissions.entity';

import { Role} from '../database/entitys/role.entity' 

import { Privileges } from '../database/entitys/privileges.entity'


@Injectable()
export class PermissionsService {

    constructor(@InjectRepository(Permissions) private repoPermi :Repository<Permissions>,
    @InjectRepository(Role) private repoRole :Repository<Role>,
    @InjectRepository(Privileges) private repoPrivileges :Repository<Privileges>){}


    async getAll()
    {
        try {
            return await this.repoPermi.find({order : {'perm_id': 'ASC'}});
        } catch (error) {   
            return {Message: 'Error', Status: '400'}
        }
     
    }



    async findOne(perm_id)
    {
        try{
        const foundPermi = await this.repoPermi.findOne({where: {perm_id}})
        
        if(!foundPermi) return {Message: 'Permissions not found', Status: '404'}
        
        return {Message: foundPermi, Status:'201'}
        
        } catch (error) {
            return {Message: 'Error' , Status: '400'}
        }
    }




    
    /*    
        Recibe un Json con este formato:

        {
            "role_id": 1,
            "privi_id": 52,
            "perm_active": true
        }

    */

    async create(privilege)
    {
        try {
              
            const newPermi = this.repoPermi.create(privilege);

            const savePermi = await this.repoPermi.save(newPermi); 

            return {Message: savePermi , Status:'201'}

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }



    /*
        Json que recibimos para Actualizar
            {
                "perm_active": false
            }

    */

    async update(perm_id , permissions){


        //El permiso solo puede cambiar de True => False o viceversa

        try
        {
            //Encontar permiso
            const findPermi  = await this.repoPermi.findOne({where: {perm_id}});  
            if(!findPermi) return {Message: 'Permissions not found', Status: '404'} 
        
            this.repoPermi.merge(findPermi, permissions);

            const savePermi = await this.repoPermi.save(findPermi)
            return {'Message': savePermi , Status:'201'};

    
        } catch (error) {
         
            return {Message: error, Status: '400'}
        }

    }







    async delete(perm_id)
    {
        try
        {
            const deletePermi = await this.repoPermi.delete({perm_id})

            if(deletePermi.affected == 0) return {Message: 'Permissions not found', Status: '404'}
        
            return {'Message': 'Permissions deleted', Status:'201'};
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }




}
