import { Module } from '@nestjs/common';
import { PermissionsController } from './permissions.controller';
import { PermissionsService } from './permissions.service';


import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from '../database/entitys/role.entity';
import { Permissions } from '../database/entitys/permissions.entity';
import { Privileges } from 'src/database/entitys/privileges.entity'; 




@Module({
  imports:[TypeOrmModule.forFeature([Role,Permissions,Privileges])],
  controllers: [PermissionsController],
  providers: [PermissionsService]
})
export class PermissionsModule {}
