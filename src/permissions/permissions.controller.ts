import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';



import { PermissionsService } from './permissions.service';

@Controller('permissions')
export class PermissionsController {

    constructor ( private PermissionsService : PermissionsService) {}


    @Get()
    async get()
    {
        return await this.PermissionsService.getAll();
    }


    @Get('/:perm_id')
    getbyID(@Param('perm_id') perm_id)
    {
       return this.PermissionsService.findOne(perm_id);
    }



    @Post()
    async create(@Body() permissions)
    {
        return await this.PermissionsService.create(permissions);
    }



    @Put('/:perm_id')
    async update(@Param('perm_id') perm_id, @Body() permissions){

        return await this.PermissionsService .update(perm_id, permissions)

    }



    @Delete('/:perm_id')
    async delete(@Param('perm_id') perm_id)
    {
        return await this.PermissionsService.delete(perm_id)
    }



}
