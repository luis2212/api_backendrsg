import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Labels } from 'src/database/entitys/labels.entity';
import { Scrum_card } from 'src/database/entitys/scrum_card.entity';
import { Users } from 'src/database/entitys/users.entity';
import { ScrumCardController } from './scrum-card.controller';
import { ScrumCardService } from './scrum-card.service';

@Module({
  imports: [TypeOrmModule.forFeature([Scrum_card, Users, Labels])],
  controllers: [ScrumCardController],
  providers: [ScrumCardService],
})
export class ScrumCardModule {}
