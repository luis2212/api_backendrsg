import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Labels } from 'src/database/entitys/labels.entity';
import { Scrum_card } from 'src/database/entitys/scrum_card.entity';
import { Users } from 'src/database/entitys/users.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ScrumCardService {
  constructor(
    @InjectRepository(Scrum_card)
    private scrum_cardRepository: Repository<Scrum_card>,
    @InjectRepository(Users)
    private userRepository: Repository<Users>,
    @InjectRepository(Labels)
    private labelsRepository: Repository<Labels>,
  ) {}

  async getAllScrumCard(): Promise<Scrum_card[]> {
    return this.scrum_cardRepository.find();
  }

  async getOneScrumCard(id_scard: number): Promise<Scrum_card> {
    return this.scrum_cardRepository.findOne(id_scard);
  }

  async createScrumCard(body: Scrum_card) {
    try {
      const newScrumCard = await this.scrum_cardRepository.create(body);
      if (body.keppers && body.keppers.length > 0) {
        let Users = [];

        for (let i = 0; i < body.keppers.length; i++) {
          const idUser = body.keppers[i];
          const newUser = await this.userRepository.findOne(idUser);
          if (!newUser) return { Message: 'User not found', Status: 404 };
          Users.push(newUser);
        }
        newScrumCard.keppers = Users;
      }
      if (body.labels && body.labels.length > 0) {
        let Labels = [];

        for (let i = 0; i < body.labels.length; i++) {
          const idLabel = body.labels[i];
          const newLabel = await this.labelsRepository.findOne(idLabel);
          if (!newLabel) return { Message: 'Label not found', Status: 404 };
          Labels.push(newLabel);
        }
        newScrumCard.labels = Labels;
      }

      const scrumCardCreated = await this.scrum_cardRepository.save(
        newScrumCard,
      );

      return {
        Message: 'Scrum card created successfully',
        ScrumCard: scrumCardCreated,
        Status: 201,
      };
    } catch (error) {
      return { Message: 'Error creating Scrum card', Error: error.message };
    }
  }

  async deleteScrumCard(id_scard: number) {
    try {
      const ScardDelete = await this.scrum_cardRepository.delete(id_scard);
      return { Message: 'Scrum card deleted successfully', Status: 201 };
    } catch (err) {
      return { Message: 'Error deleting Scrum card', Error: err.message };
    }
  }

  async updateScrumCard(id_scard: number, body: Scrum_card) {
    try {
      const foundScard = await this.scrum_cardRepository.findOne(id_scard);
      !foundScard
        ? { Message: 'Scrum card not found', Status: 404 }
        : await this.scrum_cardRepository.merge(foundScard, body);

      if (body.labels && body.labels.length > 0) {
        let Labels = [];

        for (let i = 0; i < body.labels.length; i++) {
          const idLabel = body.labels[i];
          const newLabel = await this.labelsRepository.findOne(idLabel);
          if (!newLabel) return { Message: 'Labels not found', Status: 404 };
          Labels.push(newLabel);
        }
        foundScard.labels = Labels;
      }
      const updatedScard = await this.scrum_cardRepository.save(foundScard);
      return {
        Message: 'Updated Scrum card',
        Scard: updatedScard,
        Status: 201,
      };
    } catch (err) {
      return {
        Message: 'Error updating Scrum card',
        Error: err.message,
        Status: 400,
      };
    }
  }
}
