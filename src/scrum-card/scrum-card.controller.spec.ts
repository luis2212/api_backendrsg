import { Test, TestingModule } from '@nestjs/testing';
import { ScrumCardController } from './scrum-card.controller';

describe('ScrumCardController', () => {
  let controller: ScrumCardController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ScrumCardController],
    }).compile();

    controller = module.get<ScrumCardController>(ScrumCardController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
