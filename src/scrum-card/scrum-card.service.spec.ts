import { Test, TestingModule } from '@nestjs/testing';
import { ScrumCardService } from './scrum-card.service';

describe('ScrumCardService', () => {
  let service: ScrumCardService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ScrumCardService],
    }).compile();

    service = module.get<ScrumCardService>(ScrumCardService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
