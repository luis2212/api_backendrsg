import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { Scrum_card } from 'src/database/entitys/scrum_card.entity';
import { ScrumCardService } from './scrum-card.service';

@Controller('scrum-card')
export class ScrumCardController {
  constructor(
    private scrumCardService: ScrumCardService,
  ){}

  @Get()
  async getAllScrumCard(): Promise<Scrum_card[]>{
    return await this.scrumCardService.getAllScrumCard();
  }

  @Get('/:id')
  async getOneScrumCard(@Param('id') id: number): Promise<Scrum_card>{
    return await this.scrumCardService.getOneScrumCard(id);
  }

  @Post('/')
  async createScrumCard(@Body() body: Scrum_card){
    return await this.scrumCardService.createScrumCard(body);
  }

  @Delete('/:id')
  async deleteScrumCard(@Param('id') id: number){
    return await this.scrumCardService.deleteScrumCard(id);
  }

  @Put('/:id')
  async updateScrumCard(@Param('id') id: number, @Body() body: Scrum_card){
    return await this.scrumCardService.updateScrumCard(id, body);
  }
}
