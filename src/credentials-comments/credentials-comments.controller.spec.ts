import { Test, TestingModule } from '@nestjs/testing';
import { CredentialsCommentsController } from './credentials-comments.controller';

describe('CredentialsCommentsController', () => {
  let controller: CredentialsCommentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CredentialsCommentsController],
    }).compile();

    controller = module.get<CredentialsCommentsController>(CredentialsCommentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
