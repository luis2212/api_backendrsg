import { Test, TestingModule } from '@nestjs/testing';
import { CredentialsCommentsService } from './credentials-comments.service';

describe('CredentialsCommentsService', () => {
  let service: CredentialsCommentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CredentialsCommentsService],
    }).compile();

    service = module.get<CredentialsCommentsService>(CredentialsCommentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
