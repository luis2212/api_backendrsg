import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Credentials_comments } from 'src/database/entitys/credentials_comments.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CredentialsCommentsService {
  constructor(
    @InjectRepository(Credentials_comments)
    private Credentials_commentsRepository: Repository<Credentials_comments>,
  ) {}

  async getAllCredentialsComments(): Promise<Credentials_comments[]> {
    try {
      return await this.Credentials_commentsRepository.find();
    } catch (e) {
      return e.message;
    }
  }

  async getOneCredentialsComment(
    id_credential_comment: number,
  ): Promise<Credentials_comments> {
    try {
      return await this.Credentials_commentsRepository.findOne(
        id_credential_comment,
      );
    } catch (e) {
      return e.message;
    }
  }

  async createCredentialsComments(body: Credentials_comments) {
    try {
      const newCredentialsComment =
        await this.Credentials_commentsRepository.create(body);
      const CredentialsCommentsCreate =
        await this.Credentials_commentsRepository.save(newCredentialsComment);
      return {
        Message: 'Credentials comments create successfully',
        CredentialsCommentsCreate: CredentialsCommentsCreate,
        Status: 201,
      };
    } catch (error) {
      return { Message: 'Credentials comments create failed', Error: error };
    }
  }

  async deleteCredentialsComments(id_credential_comment: number) {
    try {
      await this.Credentials_commentsRepository.delete(id_credential_comment);
      return {
        Message: 'Credentials Comment deleted successfully',
        Status: 201,
      };
    } catch (error) {
      return {
        Message: 'Credentials Comment deleted failed',
        Error: error.Message,
        Status: 400,
      };
    }
  }

  async updateCredentialsComments(
    id_credential_comment: number,
    body: Credentials_comments,
  ) {
    try {
      const foundCredentialsComment =
        await this.Credentials_commentsRepository.findOne(
          id_credential_comment,
        );
      !foundCredentialsComment
        ? { Message: 'Credentials comment not found', Status: 404 }
        : await this.Credentials_commentsRepository.merge(
            foundCredentialsComment,
            body,
          );

      const updatedCredentialsComment =
        await this.Credentials_commentsRepository.save(foundCredentialsComment);

      return {
        Message: 'Updated credentials comments successfully',
        newCredentialsComment: updatedCredentialsComment,
        Status: 201,
      };
    } catch (error) {
      return {
        Message: 'Credentials Comment updated failed',
        Error: error.Message,
        Status: 400,
      };
    }
  }
}
