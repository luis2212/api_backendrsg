import {
  Body,
  Controller,
  Delete,
  Get,
  Injectable,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Credentials_comments } from 'src/database/entitys/credentials_comments.entity';
import { CredentialsCommentsService } from './credentials-comments.service';

@Controller('credentials-comments')
export class CredentialsCommentsController {
  constructor(private credentialsCommentsService: CredentialsCommentsService) {}

  @Get('/')
  async getAllCredentialsComments(): Promise<Credentials_comments[]> {
    return await this.credentialsCommentsService.getAllCredentialsComments();
  }

  @Get('/:id')
  async getOneCredentialsComments(
    @Param('id') id: number,
  ): Promise<Credentials_comments> {
    return await this.credentialsCommentsService.getOneCredentialsComment(id);
  }

  @Post('/')
  async createCredentialsComments(@Body() body: Credentials_comments) {
    return await this.credentialsCommentsService.createCredentialsComments(
      body,
    );
  }

  @Delete('/:id')
  async deleteCredentialsComments(@Param('id') id: number) {
    return await this.credentialsCommentsService.deleteCredentialsComments(id);
  }

  @Put('/:id')
  async updateCredentialsComments(
    @Param('id') id: number,
    @Body() body: Credentials_comments,
  ) {
    return await this.credentialsCommentsService.updateCredentialsComments(
      id,
      body,
    );
  }
}
