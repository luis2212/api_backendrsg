import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Credentials_comments } from 'src/database/entitys/credentials_comments.entity';
import { CredentialsCommentsController } from './credentials-comments.controller';
import { CredentialsCommentsService } from './credentials-comments.service';

@Module({
  imports: [TypeOrmModule.forFeature([Credentials_comments])],
  controllers: [CredentialsCommentsController],
  providers: [CredentialsCommentsService],
})
export class CredentialsCommentsModule {}
