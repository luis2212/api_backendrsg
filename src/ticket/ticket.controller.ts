import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Ticket } from 'src/database/entitys/ticket.entity';
import { TicketService } from './ticket.service';

@Controller('ticket')
export class TicketController {
  constructor(private ticketService: TicketService) {}

  @Get('/')
  async getTicket(): Promise<Ticket[]> {
    return await this.ticketService.getAllTicket();
  }
  @Get('/:id')
  async getTicketById(@Param('id') id: number) {
    return await this.ticketService.findOneTicket(id);
  }

  @Post('/')
  async createTicket(@Body() body: Ticket) {
    return await this.ticketService.createTicket(body);
  }
  @Delete('/:id')
  async deleteTicket(@Param('id') id: number) {
    return await this.ticketService.deleteTicket(id);
  }

  @Put('/:id')
  async updateTicket(@Param('id') id: number, @Body() ticket: Ticket) {
    return await this.ticketService.updateTicket(id, ticket);
  }
}
