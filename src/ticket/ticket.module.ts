import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ticket } from 'src/database/entitys/ticket.entity';
import { Users } from 'src/database/entitys/users.entity';
import { TicketController } from './ticket.controller';
import { TicketService } from './ticket.service';

@Module({
  imports: [TypeOrmModule.forFeature([Ticket, Users])],
  controllers: [TicketController],
  providers: [TicketService]
})
export class TicketModule {}
