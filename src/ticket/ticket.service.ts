import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Ticket } from 'src/database/entitys/ticket.entity';
import { Users } from 'src/database/entitys/users.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TicketService {
  constructor(
    @InjectRepository(Ticket)
    private ticketRepository: Repository<Ticket>,
    @InjectRepository(Users)
    private usersRepository: Repository<Users>,
  ) {}

  async getAllTicket(): Promise<Ticket[]> {
    return await this.ticketRepository.find();
  }

  async findOneTicket(id_ticket) {
    try {
      const ticketFound = await this.ticketRepository.findOne(id_ticket);
      return !ticketFound
        ? { Message: 'No tickets found', Status: 404 }
        : { Ticket: ticketFound, Status: 201 };
    } catch (err) {
      return { Message: 'Something went wrong', Status: 400 };
    }
  }

  async createTicket(Ticket: Ticket) {
    try {
      const newTicket = await this.ticketRepository.create(Ticket);
      
      if (Ticket.attendees && Ticket.attendees.length > 0) {
        let Users = [];
        
        for (let i = 0; i < Ticket.attendees.length; i++) {
          const idUser = Ticket.attendees[i];
          const newUser = await this.usersRepository.findOne(idUser);
          if (!newUser) return { Message: 'User not found', Status: 404 };
          Users.push(newUser);
        }
        newTicket.attendees = Users;
      }

      const saveTicket = await this.ticketRepository.save(newTicket);

      return { TicketCreated: saveTicket, Status: 201 };
    } catch (err) {
      return { Message: err.message, Status: 400 };
    }
  }

  async deleteTicket(id_ticket: number) {
    try {
      const deleteTicket = await this.ticketRepository.delete(id_ticket);
      return { Message: 'ticket deleted successfully', Status: 200 };
    } catch (err) {
      return { Message: err.message, Status: 400 };
    }
  }

  async updateTicket(id_ticket: number, Ticket: Ticket) {
    try {
      const foundTicket = await this.ticketRepository.findOne(id_ticket);

      if (Ticket.attendees && Ticket.attendees.length > 0) {
        let Users = [];
        
        for (let i = 0; i < Ticket.attendees.length; i++) {
          const idUser = Ticket.attendees[i];
          const newUser = await this.usersRepository.findOne(idUser);
          if (!newUser) return { Message: 'User not found', Status: 404 };
          Users.push(newUser);
        }
        foundTicket.attendees = Users;
      }

      !foundTicket
        ? { Message: 'Ticket not found', Status: 404 }
        : this.ticketRepository.merge(foundTicket, Ticket);

      const updateTicket = await this.ticketRepository.save(foundTicket);
      return {
        Message: 'Ticket Uploaded: ',
        TicketUploaded: updateTicket,
        Status: 200,
      };
    } catch (err) {
      return { Message: err.message, Status: 400 };
    }
  }
}
