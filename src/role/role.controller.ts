import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { RoleService } from './role.service';

@Controller('role')
export class RoleController {

    constructor ( private roleService : RoleService) {}

    @Get()
    async get()
    {
        return await this.roleService .getAll();
    }

    @Get('/:role_id')
    getbyID(@Param('role_id') role_id)
    {
       return this.roleService.findOne(role_id);
    }


    @Post()
    async create(@Body() role)
    {
        return await this.roleService.create(role);
    }



    @Put('/:role_id')
    async update(@Param('role_id') role_id, @Body() Role){

        return await this.roleService.update(role_id, Role)

    }


    @Delete('/:role_id')
    async delete(@Param('role_id') role_id)
    {
        return await this.roleService.delete(role_id)
    }


}
