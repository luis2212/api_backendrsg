import { Injectable } from '@nestjs/common';
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';
import {Role} from '../database/entitys/role.entity';

@Injectable()
export class RoleService {
 
    constructor(@InjectRepository(Role) private repoRole:Repository<Role>){}

    async getAll()
    {
       try
        {
          return await this.repoRole.find({order : {'role_id': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }

    async findOne(role_id)
    {
        try{
        const foundrole = await this.repoRole.findOne(role_id)
        
        if(!foundrole) return {Message: 'Role not found', Status: '404'}
        
        return {Message: foundrole, Status:'201'}
        

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }

    }


    /*
    Jso
    {
        "role_code": "Prueba",
        "role_name": "Prueba",
        "role_active": true
    }

    */

    async create(role)
    {
        try {  
            const newRole= this.repoRole.create(role);
            const saveRole= await this.repoRole.save(newRole);
            
            
            return {Message: saveRole , Status:'201'}
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }


    async update(role_id,role){

        try
        {
            const findRole  = await this.repoRole.findOne(role_id);
            
            if(!findRole) return {Message: 'Role not found', Status: '404'}

            this.repoRole.merge(findRole, role);

            //Objeto Actualizado
            const saveRole = await this.repoRole.save(findRole)
            return {'Message': saveRole , Status:'201'};

        
        } catch (error) {
         
            return {Message: 'Error', Status: '400'}
        }

    }


    async delete(role_id)
    {
        try
        {
            const deleteRole = await this.repoRole.delete(role_id);

            if(deleteRole.affected == 0) return {Message: 'Role not found', Status: '404'}
        
            return {'Message': 'Role deleted', Status:'201'};
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }



}
