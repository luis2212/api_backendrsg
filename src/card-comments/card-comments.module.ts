import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Card_comments } from 'src/database/entitys/card_comments.entity';
import { CardCommentsController } from './card-comments.controller';
import { CardCommentsService } from './card-comments.service';

@Module({
  imports:[TypeOrmModule.forFeature([Card_comments])],
  controllers: [CardCommentsController],
  providers: [CardCommentsService]
})
export class CardCommentsModule {}
