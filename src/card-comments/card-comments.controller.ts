import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Card_comments } from 'src/database/entitys/card_comments.entity';
import { CardCommentsService } from './card-comments.service';

@Controller('card-comments')
export class CardCommentsController {
  constructor(private cardCommentService: CardCommentsService) {}

  @Get()
  async getAllComments(): Promise<Card_comments[]> {
    return await this.cardCommentService.getAllComments();
  }

  @Get('/:id')
  async getOneComment(@Param('id') id: number): Promise<Card_comments> {
    return await this.cardCommentService.getOneComment(id);
  }

  @Post('/')
  async createComment(@Body() comment: Card_comments) {
    return await this.cardCommentService.createComment(comment);
  }

  @Delete('/:id')
  async deleteComment(@Param('id') id: number) {
    return await this.cardCommentService.deleteComment(id);
  }

  @Put('/:id')
  async updateComment(@Param('id') id: number, @Body() comment: Card_comments) {
    return await this.cardCommentService.updateComment(id, comment);
  }
}
