import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Card_comments } from 'src/database/entitys/card_comments.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CardCommentsService {
  constructor(
    @InjectRepository(Card_comments)
    private card_commentsRepository: Repository<Card_comments>,
  ) {}

  async getAllComments(): Promise<Card_comments[]> {
    return this.card_commentsRepository.find();
  }

  async getOneComment(id_comment: number): Promise<Card_comments> {
    return this.card_commentsRepository.findOne(id_comment);
  }

  async createComment(comment: Card_comments) {
    try {
      const newComment = await this.card_commentsRepository.create(comment);
      const saveComment = await this.card_commentsRepository.save(newComment);

      return {
        Message: 'Comment created successfully',
        Status: 201,
        Comment: saveComment,
      };
    } catch (err) {
      return {
        Message: 'Error creating comment',
        Error: err.message,
        Status: 400,
      };
    }
  }

  async deleteComment(id_comment: number) {
    try {
      const deleteComment = await this.card_commentsRepository.delete(
        id_comment,
      );
      return { Message: 'Comment deleted successfully', Status: 201 };
    } catch (err) {
      return {
        Message: 'Error deleted comment',
        Error: err.message,
        Status: 400,
      };
    }
  }

  async updateComment(id_comment: number, comment: Card_comments) {
    try {
      const foundComment = await this.card_commentsRepository.findOne(
        id_comment,
      );

      !foundComment
        ? { Message: 'No comment found', Status: 404 }
        : await this.card_commentsRepository.merge(foundComment, comment);
      const saveComment = await this.card_commentsRepository.save(foundComment);

      return {
        Message: 'Comment updated',
        Status: 201,
        NewComment: saveComment,
      };
    } catch (err) {
      return {
        Message: 'Error updating comment',
        Error: err.message,
        Status: 400,
      };
    }
  }
}
