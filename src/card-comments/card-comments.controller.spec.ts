import { Test, TestingModule } from '@nestjs/testing';
import { CardCommentsController } from './card-comments.controller';

describe('CardCommentsController', () => {
  let controller: CardCommentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CardCommentsController],
    }).compile();

    controller = module.get<CardCommentsController>(CardCommentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
