import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';


import { PrivilegesService } from './privileges.service';


@Controller('privileges')
export class PrivilegesController {

    constructor ( private privileService : PrivilegesService) {}


    @Get()
    async get()
    {
        return await this.privileService.getAll();
    }



    @Get('/:privi_id')
    getbyID(@Param('privi_id') privi_id)
    {
       return this.privileService.findOne(privi_id);
    }



    @Get('/group/:privig_id')
    getbyGroup(@Param('privig_id') privig_id)
    {
       return this.privileService.findGroup(privig_id);
    }



    @Post()
    async create(@Body() privilege)
    {
        return await this.privileService.create(privilege);
    }




    @Put('/:privi_id')
    async update(@Param('privi_id') privi_id, @Body() privilege){

        return await this.privileService.update(privi_id, privilege)

    }



    @Delete('/:privi_id')
    async delete(@Param('privi_id') privi_id)
    {
        return await this.privileService .delete(privi_id)
    }






}
