import { Injectable } from '@nestjs/common';


//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';

//Entidades
import { Privileges } from '../database/entitys/privileges.entity';

import {Privilege_groups} from '../database/entitys/privilege_groups.entity'



@Injectable()
export class PrivilegesService {

        
    constructor(@InjectRepository(Privileges) private repoPrivi :Repository<Privileges>,
                @InjectRepository(Privilege_groups) private repoPriviGroup :Repository<Privilege_groups>){}


    async getAll()
    {
        try {
            return await this.repoPrivi.find({order: {'privi_id':'ASC'}});
        } catch (error) {   
            return {Message: 'Error', Status: '400'}
        }
     
    }



    async findOne(privi_id)
    {
        try{
        const foundPrivilege = await this.repoPrivi.findOne(privi_id)
        
        if(!foundPrivilege) return {Message: 'Privilege not found', Status: '404'}
        
        return {Message: foundPrivilege, Status:'201'}
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }



    async findGroup(privig_id)
    {
        try{
        const foundPrivilege = await this.repoPrivi.find({where :{privilege_groups:privig_id}})
        
        if(!foundPrivilege) return {Message: 'Group Privilege not found', Status: '404'}
        
        return {Message: foundPrivilege, Status:'201'}
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }








        /*    
        Recibe un Json con este formato:

        {
            "privi_code": "PRUE01",
            "privi_name": "Acceso a plataforma",
            "privi_active": true,
            "privilege_groups": 1
        }

        */


    async create(privilege)
    {
        try {
              
            const newPrivilege = this.repoPrivi.create(privilege);

            const savePrivilege = await this.repoPrivi.save(newPrivilege); 

            return {Message: savePrivilege , Status:'201'}

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
    }





    async update(privi_id, privilege){

        try
        {
            const findPrivilege  = await this.repoPrivi.findOne(privi_id);
            
           
            if(!findPrivilege) return {Message: 'Privilege not found', Status: '404'}


            if(!privilege.privilege_groups) return {Message: 'Invalid data', Status: '400'}


            const group = await this.repoPriviGroup.findOne(privilege.privilege_groups)


            if(!group) return {Message: 'Privilege Group not valid', Status: '404'}
            
           
            findPrivilege.privilege_groups = group  
            this.repoPrivi.merge(findPrivilege, privilege);

            
            //Objeto Actualizado
            const savePrivilege = await this.repoPrivi.save(findPrivilege)
            return {'Message': savePrivilege, Status:'201'};

        } catch (error) {
         
            return {Message: 'Error', Status: '400'}
        }
    }





    async delete(privi_id)
    {
        try
        {
            const deletePrivilegeGroup = await this.repoPrivi.delete(privi_id);

            if(deletePrivilegeGroup.affected == 0) return {Message: 'Privilege not found', Status: '404'}
        
            return {'Message': 'Privilege deleted', Status:'201'};
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }
        
    }




}
