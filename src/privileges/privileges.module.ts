import { Module } from '@nestjs/common';
import { PrivilegesController } from './privileges.controller';
import { PrivilegesService } from './privileges.service';


import { TypeOrmModule } from '@nestjs/typeorm';
import { Privileges } from '../database/entitys/privileges.entity';

import { Privilege_groups } from '../database/entitys/privilege_groups.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Privileges, Privilege_groups])],
  controllers: [PrivilegesController],
  providers: [PrivilegesService]
})
export class PrivilegesModule {}
