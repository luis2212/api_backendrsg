import { Test, TestingModule } from '@nestjs/testing';
import { PrivilegeGroupsService } from './privilege-groups.service';

describe('PrivilegeGroupsService', () => {
  let service: PrivilegeGroupsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PrivilegeGroupsService],
    }).compile();

    service = module.get<PrivilegeGroupsService>(PrivilegeGroupsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
