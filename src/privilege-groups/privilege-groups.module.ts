import { Module } from '@nestjs/common';
import { PrivilegeGroupsController } from './privilege-groups.controller';
import { PrivilegeGroupsService } from './privilege-groups.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Privilege_groups } from '../database/entitys/privilege_groups.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Privilege_groups])],
  controllers: [PrivilegeGroupsController],
  providers: [PrivilegeGroupsService],
})
export class PrivilegeGroupsModule {}
