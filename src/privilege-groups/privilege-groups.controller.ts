import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';

import { PrivilegeGroupsService } from './privilege-groups.service';


@Controller('privilege_groups')
export class PrivilegeGroupsController {

    constructor ( private privilegeGroupService : PrivilegeGroupsService) {}

    @Get()
    async get()
    {
        return await this.privilegeGroupService.getAll();
    }

    @Get('/:privig_id')
    getbyID(@Param('privig_id') privig_id)
    {
       return this.privilegeGroupService.findOne(privig_id);
    }


    @Post()
    async create(@Body() privilege_group)
    {
        return await this.privilegeGroupService.create(privilege_group);
    }



    @Delete('/:privig_id')
    async delete(@Param('privig_id')  privig_id)
    {
        return await this.privilegeGroupService.delete(privig_id)
    }



    @Put('/:privig_id')
    async update(@Param('privig_id') privig_id, @Body() privilege_group){

        return await this.privilegeGroupService.update(privig_id, privilege_group)

    }


   


}
