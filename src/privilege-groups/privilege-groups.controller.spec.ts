import { Test, TestingModule } from '@nestjs/testing';
import { PrivilegeGroupsController } from './privilege-groups.controller';

describe('PrivilegeGroupsController', () => {
  let controller: PrivilegeGroupsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PrivilegeGroupsController],
    }).compile();

    controller = module.get<PrivilegeGroupsController>(
      PrivilegeGroupsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
