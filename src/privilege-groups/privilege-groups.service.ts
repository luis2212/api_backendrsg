import { Injectable } from '@nestjs/common';

//Librerias
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

//Entidades
import { Privilege_groups } from '../database/entitys/privilege_groups.entity';

@Injectable()
export class PrivilegeGroupsService {
  
  constructor(
    @InjectRepository(Privilege_groups) private repoPriviGrupo: Repository<Privilege_groups>) {}

  
    async getAll()
    {
       try
        {
          return await this.repoPriviGrupo.find({order : {'privig_id': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }


    async findOne(privig_id)
    {
        try{
        const foundPrivilegeGroup = await this.repoPriviGrupo.findOne(privig_id)
        
        if(!foundPrivilegeGroup) return {Message: 'Privilege Group not found', Status: '404'}
        
      
        return { Message: foundPrivilegeGroup, Status: '201' };
      } catch (error) {
        return { Message: 'Error', Status: '400' };
      }
    }
  

  /*
    Recibe un Json con este formato
        {
             "privig_name": "General",
             "privig_position": 1
        }
    */

  async create(privilege_group) {
    try {
      const newPrivilegeGroup = this.repoPriviGrupo.create(privilege_group);

      const savePrivilegeGroup = await this.repoPriviGrupo.save(
        newPrivilegeGroup,
      );

      return { Message: savePrivilegeGroup, Status: '201' };
    } catch (error) {
      return { Message: 'Error', Status: '400' };
    }
  }

  async delete(privig_id) {
    try {
      const deletePrivilegeGroup = await this.repoPriviGrupo.delete(privig_id);

      if (deletePrivilegeGroup.affected == 0)
        return { Message: 'Privilege Group not found', Status: '404' };

      return { Message: 'Privilege Group deleted', Status: '201' };
    } catch (error) {
      return { Message: 'Error', Status: '400' };
    }
  }

  async update(privig_id, privilege_group) {
    try {
      const findPrivilegeGroup = await this.repoPriviGrupo.findOne(privig_id);

      if (!findPrivilegeGroup)
        return { Message: 'Privilege Group not found', Status: '404' };

      this.repoPriviGrupo.merge(findPrivilegeGroup, privilege_group);

      //Objeto Actualizado
      const savePrivilegeGroup = await this.repoPriviGrupo.save(
        findPrivilegeGroup,
      );

      return { Message: savePrivilegeGroup, Status: '201' };
    } catch (error) {
      return { Message: 'Error', Status: '400' };
    }
  }
  
}
