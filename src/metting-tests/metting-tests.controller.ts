import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';

import { MettingTestsService } from './metting-tests.service';

@Controller('metting_tests')
export class MettingTestsController {

    constructor ( private mettingService : MettingTestsService) {}


    @Get()
    async get()
    {
        return await this.mettingService.getAll();
    }


  
    @Get('/:id_metting')
    getbyID(@Param('id_metting') id_metting)
    {
       return this.mettingService.findOne(id_metting);
    }





    //Se recibe el ID del Board   "Ver todas las reuniones de un proyecto"
    @Get('board/:id_board')
    getbyBoard(@Param('id_board') id_board)
    {
       return this.mettingService.findMettingBoard(id_board);
    }



    @Post()
    async create(@Body() Metting)
    {
        return await this.mettingService.create(Metting);
    }



    @Put('/:id_metting')
    async update(@Param('id_metting') id_metting, @Body() metting){

        return await this.mettingService.update(id_metting, metting)

    }



    @Delete('/:id_metting')
    async delete(@Param('id_metting') id_metting)
    {
        return await this.mettingService.delete(id_metting)
    }


}
