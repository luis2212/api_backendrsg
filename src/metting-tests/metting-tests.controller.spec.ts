import { Test, TestingModule } from '@nestjs/testing';
import { MettingTestsController } from './metting-tests.controller';

describe('MettingTestsController', () => {
  let controller: MettingTestsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MettingTestsController],
    }).compile();

    controller = module.get<MettingTestsController>(MettingTestsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
