import { Module } from '@nestjs/common';
import { MettingTestsController } from './metting-tests.controller';
import { MettingTestsService } from './metting-tests.service';



import { TypeOrmModule } from '@nestjs/typeorm';
import { Metting_tests } from '../database/entitys/metting_tests.entity';
import {Board} from '../database/entitys/board.entity'
import { Users } from 'src/database/entitys/users.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Board, Metting_tests, Users])],
  controllers: [MettingTestsController],
  providers: [MettingTestsService]
})
export class MettingTestsModule {}
