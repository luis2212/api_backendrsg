import { Injectable } from '@nestjs/common';

//Librerias
import { InjectRepository} from '@nestjs/typeorm';
import { Repository} from 'typeorm';

//Entidades
import { Metting_tests } from '../database/entitys/metting_tests.entity';
import {Board} from '../database/entitys/board.entity'
import { Users } from 'src/database/entitys/users.entity';


@Injectable()
export class MettingTestsService {

    constructor(@InjectRepository(Board) private repoBoard:Repository<Board>,
    @InjectRepository(Metting_tests) private repoMetting:Repository<Metting_tests>,
    @InjectRepository(Users) private repoUsers:Repository<Users>){}



    
    async getAll()
    {
       try
        {
          return await this.repoMetting.find({order : {'id_metting': 'ASC'}});
        } catch (error) {   
          return {Message: 'Error', Status: '400'}
        }
    }



    async findOne(id_metting)
    {
        try{
        const foundrole = await this.repoMetting.findOne(id_metting)
        if(!foundrole) return {Message: 'Metting not found', Status: '404'}
        return {Message: foundrole, Status:'201'}

        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }

    }



    //Todas la reuniones de un Board
            // Ruta ->  http://localhost:3000/metting_tests/board/1
    async findMettingBoard(id_board)
    {
        try{
        //Buscamos Board
        const findBoard = await this.repoBoard.findOne(id_board)
        if(!findBoard) return {Message: 'Board not found', Status: '404'}

        const foundFavorites = await this.repoMetting.find({where:{board:findBoard}, order : {'id_metting': 'ASC'}})
       
        return {Message: foundFavorites, Status:'201'}
        
        } catch (error) {
            return {Message: 'Error', Status: '400'}
        }

    }





    /*
    JSON:
       { 
            "title_metting": "Crear Usuarios" , 
            "description_metting": "Se deberan crear usuarios para todos",
            "scheduled_date": "2021-10-22" ,
            "start_time": "2021-10-22 20:30:00",
            "end_time": "2021-10-22 21:30:00", 
            "platform": "Temas",
            "memorandum": "Se llego a un acuerdo",
            "archived_metting": false,
            "id_board": 4
        }   

    */


        async create(Metting)
        {
            try {  
    
                //Desestrucutramos
                const newMetting = new Metting_tests
                newMetting.title_metting = Metting.title_metting
                newMetting.description_metting = Metting.description_metting
                newMetting.scheduled_date = Metting.scheduled_date
                newMetting.start_time = Metting.start_time
                newMetting.end_time = Metting.end_time
                newMetting.platform = Metting.platform
                newMetting.memorandum = Metting.memorandum
                newMetting.archived_metting = Metting.archived_metting
                //attendees
                if (Metting.attendees && Metting.attendees.length > 0) {
                    let Users = [];
                    
                    for (let i = 0; i < Metting.attendees.length; i++) {
                      const idUser = Metting.attendees[i];
                      const newUser = await this.repoUsers.findOne(idUser);
                      if (!newUser) return { Message: 'User not found', Status: 404 };
                      Users.push(newUser);
                    }
                    newMetting.attendees = Users;
                  }

                //Buscamos Board
                const findBoard = await this.repoBoard.findOne(Metting.id_board)
                if(!findBoard) return {Message: 'Board not found', Status: '404'}
                newMetting.board = findBoard
    
                const saveMetting= await this.repoMetting.save(newMetting);
            
                return {Message: saveMetting , Status:'201'}
            } catch (error) {
                return {Message: 'Error', Status: '400'}
            }
        }
    
    

        async update(id_metting, Metting){

            try
            {
                const findMetting  = await this.repoMetting.findOne(id_metting);            
                if(!findMetting) return {Message: 'Metting not found', Status: '404'}
    

                //Desestrucutramos
                const newMetting = new Metting_tests
                newMetting.title_metting = Metting.title_metting
                newMetting.description_metting = Metting.description_metting
                newMetting.scheduled_date = Metting.scheduled_date
                newMetting.start_time = Metting.start_time
                newMetting.end_time = Metting.end_time
                newMetting.platform = Metting.platform
                newMetting.memorandum = Metting.memorandum
                newMetting.archived_metting = Metting.archived_metting
               
                if (Metting.attendees && Metting.attendees.length > 0) {
                    let Users = [];
                    
                    for (let i = 0; i < Metting.attendees.length; i++) {
                      const idUser = Metting.attendees[i];
                      const newUser = await this.repoUsers.findOne(idUser);
                      if (!newUser) return { Message: 'User not found', Status: 404 };
                      Users.push(newUser);
                    }
                    newMetting.attendees = Users;
                  }
                //Buscamos Board
                const findBoard = await this.repoBoard.findOne(Metting.id_board)
                if(!findBoard) return {Message: 'Board not found', Status: '404'}
                newMetting.board = findBoard
    
    
                this.repoMetting.merge(findMetting,  newMetting);
    
                //Objeto Actualizado
                const saveMetting = await this.repoMetting.save(findMetting)
                return {'Message': saveMetting , Status:'201'};
    
            
            } catch (error) {
             
                return {Message: 'Error', Status: '400'}
            }
    
        }
    




    
        async delete(id_metting)
        {
            try
            {
                const deleteMetting = await this.repoMetting.delete(id_metting);
    
                if(deleteMetting.affected == 0) return {Message: 'Metting not found', Status: '404'}
            
                return {'Message': 'Metting deleted', Status:'201'};
            
            } catch (error) {
                return {Message: 'Error', Status: '400'}
            }
            
        }
    


}
