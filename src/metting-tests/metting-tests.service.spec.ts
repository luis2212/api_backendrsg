import { Test, TestingModule } from '@nestjs/testing';
import { MettingTestsService } from './metting-tests.service';

describe('MettingTestsService', () => {
  let service: MettingTestsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MettingTestsService],
    }).compile();

    service = module.get<MettingTestsService>(MettingTestsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
